package ontology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * @author G. Razis
 */
public class Ontology {
	
	/** prefixes **/
	public static final String instancePrefix;
	public static final String eLodPrefix;
	public static final String skosPrefix;
	
	/**Classes**/
	//eLod
	public static final Resource statisticResource;
	public static final Resource aggregateResource;
	public static final Resource counterResource;
	public static final Resource rankResource;
	public static final Resource indicatorResource;
	//SKOS
	public static final Resource conceptResource;
	public static final Resource conceptSchemeResource;
	
	/**Object Properties**/
	//eLod
	public static final Property isStatisticOf;
	public static final Property hasCategory;
	public static final Property hasAggregate;
	public static final Property hasCounter;
	public static final Property hasRank;
	public static final Property hasIndicator;
	
	/**Data Properties**/
	//eLod
	public static final Property lastUpdateDate;
	public static final Property referenceTime;
	public static final Property referenceTimeDuration;
	public static final Property aggregatedAmount;
	public static final Property counter;
	public static final Property rank;
	public static final Property variation;
	public static final Property orgActivity;
	public static final Property calculationDate;
	//SKOS
	public static final Property hasTopConcept;
	public static final Property topConceptOf;
	public static final Property inScheme;
	public static final Property narrower;
	public static final Property broader;
	public static final Property prefLabel;
	
	static {
		/** prefixes **/
		instancePrefix = "http://linkedeconomy.org/resource/";
		eLodPrefix = "http://linkedeconomy.org/ontology#";
		skosPrefix = "http://www.w3.org/2004/02/skos/core#";
		
		/** Resources **/
		//eLod
		statisticResource = ResourceFactory.createResource(eLodPrefix + "Statistic");
		aggregateResource = ResourceFactory.createResource(eLodPrefix + "Aggregate");
		counterResource = ResourceFactory.createResource(eLodPrefix + "Counter");
		rankResource = ResourceFactory.createResource(eLodPrefix + "Rank");
		indicatorResource = ResourceFactory.createResource(eLodPrefix + "Indicator");
		//SKOS
		conceptResource = ResourceFactory.createResource(skosPrefix + "Concept");
		conceptSchemeResource = ResourceFactory.createResource(skosPrefix + "ConceptScheme");
		
		/** Object Properties **/
		//eLod
		isStatisticOf = ResourceFactory.createProperty(eLodPrefix + "isStatisticOf");
		hasCategory = ResourceFactory.createProperty(eLodPrefix + "hasCategory");
		hasAggregate = ResourceFactory.createProperty(eLodPrefix + "hasAggregate");
		hasCounter = ResourceFactory.createProperty(eLodPrefix + "hasCounter");
		hasRank = ResourceFactory.createProperty(eLodPrefix + "hasRank");
		hasIndicator = ResourceFactory.createProperty(eLodPrefix + "hasIndicator");
		
		/** Data Properties **/
		//elod
		lastUpdateDate = ResourceFactory.createProperty(eLodPrefix + "lastUpdateDate");
		referenceTime = ResourceFactory.createProperty(eLodPrefix + "referenceTime");
		referenceTimeDuration = ResourceFactory.createProperty(eLodPrefix + "referenceTimeDuration");
		aggregatedAmount = ResourceFactory.createProperty(eLodPrefix + "aggregatedAmount");
		counter = ResourceFactory.createProperty(eLodPrefix + "counter");
		rank = ResourceFactory.createProperty(eLodPrefix + "rank");
		variation = ResourceFactory.createProperty(eLodPrefix + "variation");
		orgActivity = ResourceFactory.createProperty(eLodPrefix + "orgActivity");
		calculationDate = ResourceFactory.createProperty(eLodPrefix + "calculationDate");
		//SKOS
		hasTopConcept = ResourceFactory.createProperty(skosPrefix + "hasTopConcept");
		topConceptOf = ResourceFactory.createProperty(skosPrefix + "topConceptOf");
		inScheme = ResourceFactory.createProperty(skosPrefix + "inScheme");
		narrower = ResourceFactory.createProperty(skosPrefix + "narrower");
		broader = ResourceFactory.createProperty(skosPrefix + "broader");
		prefLabel = ResourceFactory.createProperty(skosPrefix + "prefLabel");
	}
	
}