package ontology;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author G. Razis
 */
public class OntologyInitialization {
	
	/**
     * Add the necessary prefixes to the model we are currently 
     * working with.
     * 
     * @param Model the model we are currently working with
     */
	public void setPrefixes(Model model) {
		model.setNsPrefix("elod", Ontology.eLodPrefix);
		model.setNsPrefix("skos", Ontology.skosPrefix);
	}
	
	/**
     * Create the basic hierarchies of the OWL classes and their labels 
     * to the model we are currently working with.
     * 
     * @param Model the model we are currently working with
     */
	public void createHierarchies(Model model) {
		//Statistic
		model.add(Ontology.statisticResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.statisticResource, RDFS.label, model.createLiteral("Statistic", "en"));
		model.add(Ontology.statisticResource, RDFS.label, model.createLiteral("Στατιστικά", "el"));
				
		//Concept
		model.add(Ontology.conceptResource, RDFS.subClassOf, OWL.Thing);
		
		//Concept Scheme
		model.add(Ontology.conceptSchemeResource, RDFS.subClassOf, OWL.Thing);
		
		//Aggregate
		model.add(Ontology.aggregateResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.aggregateResource, RDFS.label, model.createLiteral("Aggregate", "en"));
		model.add(Ontology.aggregateResource, RDFS.label, model.createLiteral("Συνολικό Ποσό", "el"));
		
		//Counter
		model.add(Ontology.counterResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.counterResource, RDFS.label, model.createLiteral("Counter", "en"));
		model.add(Ontology.counterResource, RDFS.label, model.createLiteral("Μετρητής", "el"));
				
		//Rank
		model.add(Ontology.rankResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.rankResource, RDFS.label, model.createLiteral("Rank", "en"));
		model.add(Ontology.rankResource, RDFS.label, model.createLiteral("Κατάταξη", "el"));
		
		//Rank
		model.add(Ontology.indicatorResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.indicatorResource, RDFS.label, model.createLiteral("Indicator", "en"));
		model.add(Ontology.indicatorResource, RDFS.label, model.createLiteral("Δείκτης", "el"));
	}
	
	/**
     * Create the SKOS Concepts and Concept Schemes to the 
     * model we are currently working with.
     * 
     * @param Model the model we are currently working with
     */
	public void addIndividualsToModel(Model model) {
		
		/** Concepts **/
		/* regarding FpmStageScheme */
		Resource conceptTenderResource = model.createResource(Ontology.eLodPrefix + "Tender", Ontology.conceptResource);
		Resource conceptContractResource = model.createResource(Ontology.eLodPrefix + "Contract", Ontology.conceptResource);
		Resource conceptPaymentResource = model.createResource(Ontology.eLodPrefix + "Payment", Ontology.conceptResource);
		Resource conceptBudgetResource = model.createResource(Ontology.eLodPrefix + "Budget", Ontology.conceptResource);
		Resource conceptCommittedResource = model.createResource(Ontology.eLodPrefix + "Committed", Ontology.conceptResource);
		Resource conceptExpenseApprovalResource = model.createResource(Ontology.eLodPrefix + "ExpenseApproval", Ontology.conceptResource);
		Resource conceptAssignmentResource = model.createResource(Ontology.eLodPrefix + "Assignment", Ontology.conceptResource);
		Resource conceptNoticeResource = model.createResource(Ontology.eLodPrefix + "Notice", Ontology.conceptResource);
		Resource conceptAwardResource = model.createResource(Ontology.eLodPrefix + "Award", Ontology.conceptResource);
		
		/** Concept Schemes **/ 
		Resource fpmStageSchemeResource = model.createResource(Ontology.eLodPrefix + "FpmStageScheme", Ontology.conceptSchemeResource);
		
		/** configure Concept Schemes **/
		/* regarding FpmStageScheme */
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptTenderResource);
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptContractResource);
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptPaymentResource);
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptBudgetResource);
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptCommittedResource);
		fpmStageSchemeResource.addProperty(Ontology.hasTopConcept, conceptExpenseApprovalResource);
		
		conceptTenderResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		conceptContractResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		conceptPaymentResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		conceptBudgetResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		conceptCommittedResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		conceptExpenseApprovalResource.addProperty(Ontology.topConceptOf, fpmStageSchemeResource);
		
		conceptTenderResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptContractResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptPaymentResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptBudgetResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptCommittedResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptExpenseApprovalResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptAssignmentResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptNoticeResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		conceptAwardResource.addProperty(Ontology.inScheme, fpmStageSchemeResource);
		
		conceptContractResource.addProperty(Ontology.narrower, conceptAssignmentResource);
		conceptContractResource.addProperty(Ontology.narrower, conceptNoticeResource);
		conceptContractResource.addProperty(Ontology.narrower, conceptAwardResource);
		
		conceptAssignmentResource.addProperty(Ontology.broader, conceptContractResource);
		conceptNoticeResource.addProperty(Ontology.broader, conceptContractResource);
		conceptAwardResource.addProperty(Ontology.broader, conceptContractResource);
		
		/** configure prefLabels **/
		/* regarding FpmStageScheme */
		conceptTenderResource.addProperty(Ontology.prefLabel, model.createLiteral("Προκήρυξη", "el"));
		conceptContractResource.addProperty(Ontology.prefLabel, model.createLiteral("Σύμβαση", "el"));
		conceptPaymentResource.addProperty(Ontology.prefLabel, model.createLiteral("Πληρωμή", "el"));
		conceptBudgetResource.addProperty(Ontology.prefLabel, model.createLiteral("Προϋπολογισμός", "el"));
		conceptCommittedResource.addProperty(Ontology.prefLabel, model.createLiteral("Ανάληψη Υποχρέωσης", "el"));
		conceptExpenseApprovalResource.addProperty(Ontology.prefLabel, model.createLiteral("Έγκριση Δαπάνης", "el"));
		conceptAssignmentResource.addProperty(Ontology.prefLabel, model.createLiteral("Ανάθεση", "el"));
		conceptNoticeResource.addProperty(Ontology.prefLabel, model.createLiteral("Διακήρυξη", "el"));
		conceptAwardResource.addProperty(Ontology.prefLabel, model.createLiteral("Κατακύρωση", "el"));
		
		conceptTenderResource.addProperty(Ontology.prefLabel, model.createLiteral("Tender", "en"));
		conceptContractResource.addProperty(Ontology.prefLabel, model.createLiteral("Contract", "en"));
		conceptPaymentResource.addProperty(Ontology.prefLabel, model.createLiteral("Payment", "en"));
		conceptBudgetResource.addProperty(Ontology.prefLabel, model.createLiteral("Budget", "en"));
		conceptCommittedResource.addProperty(Ontology.prefLabel, model.createLiteral("Committed", "en"));
		conceptExpenseApprovalResource.addProperty(Ontology.prefLabel, model.createLiteral("Expense Approval", "en"));
		conceptAssignmentResource.addProperty(Ontology.prefLabel, model.createLiteral("Assignment", "en"));
		conceptNoticeResource.addProperty(Ontology.prefLabel, model.createLiteral("Notice", "en"));
		conceptAwardResource.addProperty(Ontology.prefLabel, model.createLiteral("Award", "en"));
	}

}