package utils;

/**
 * @author G. Razis
 */
public class QueryConfiguration {
	
	public static String queryGraphDiavgeiaII = "http://linkedeconomy.org/DiavgeiaII";
	public static final String queryGraphOrganizations = "http://linkedeconomy.org/Organizations";
	
	public static final String queryGraphDiavgeiaIIStats = "http://linkedeconomy.org/DiavgeiaIIStatistics";
	
	public static final String connectionString = "jdbc:virtuoso://143.233.226.49:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	
	public static final String username = "dba";
	public static final String password = "d3ll0lv@69";
	
	public static final String currentYear = DateMethods.getMonthsYear(); //2015 //ACR //used by Hybrids only
	public static final String dateFilterFrom = DateMethods.getMonthsFirstDate(-1); //"2015-10-01"; //ACR //previous month
	public static final String dateFilterTo = DateMethods.getMonthsFirstDate(0); //"2015-11-01"; //ACR //current month
	public static final String dateFilterPrevious = DateMethods.getMonthsFirstDate(-2); //"2015-09-01"; //Variation //previous month - 1
	public static final String dateFilterCurrent = DateMethods.getMonthsFirstDate(-1); //"2015-10-01"; //Variation //current month - 1
	
	public static String rdfName = null;
	
	public static final boolean buyers = true;
	public static final boolean sellers = false;
	public static final boolean hybrids = false;
	
	public static final int SLEEP = 10;
	
	public static final String SOURCE = "C:/Users/Makis/Desktop/eprocur/"; //"/home/makis/RDF/";
	public static final String DESTINATION = "C:/Users/Makis/Desktop"; //"/home/makis/RDF/Diavgeia2";
}