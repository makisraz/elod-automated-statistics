package utils;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import org.joda.time.LocalDate;

/**
 * @author G. Razis
 */
public class DateMethods {
	
	public static String getMonthsFirstDate(int monthsBefore) {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.MONTH, monthsBefore);
		LocalDate dt = new LocalDate( sdf.format(cal.getTime()) );
		String firstDayOfMonth = dt.dayOfMonth().withMinimumValue().toString();
		
		return firstDayOfMonth;
	}
	
	public static String getMonthsYear() {
		
		String previousMonth = getMonthsFirstDate(-1);
		String monthsYear = String.valueOf( new LocalDate(previousMonth).getYear() );
		
		return monthsYear;
	}
	
	public static String getPreviousMonth(int numOfPreviousMonths) {
		
		String previousMonth = getMonthsFirstDate(numOfPreviousMonths);
		String monthsYear = previousMonth.split("-")[1];
		
		return monthsYear;
	}

}