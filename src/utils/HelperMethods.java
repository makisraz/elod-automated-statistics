package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import ontology.Ontology;

import com.hp.hpl.jena.rdf.model.Literal;

/**
 * @author G. Razis
 */
public class HelperMethods {
	
	public String roundAmount(Literal totalAmount) {
		
		String totalAmountStr = "";
		
		if (totalAmount != null) {
			String totalAmountParts[] = totalAmount.toString().split("http");
			String amount = totalAmountParts[0].replace("^", "");
			amount = new BigDecimal(amount).toPlainString(); //handle scientific format
			try {
				String[] numParts = amount.split("\\.");
				String twoDecimalsStr = "0." + numParts[1].subSequence(0, 2).toString();
				totalAmountStr = numParts[0] + "." + twoDecimalsStr.split("\\.")[1];
			} catch (Exception e) { //Integer or Less than 3 decimals
				totalAmountStr = amount;
			}
		} else {
			totalAmountStr = "0";
		}
		
		return totalAmountStr;
	}
	
	public String roundAggregatedAmount(String amount) {
		
		String amountStr = "";
		
		try {
			String[] numParts = amount.split("\\.");
			String twoDecimalsStr = "0." + numParts[1].subSequence(0, 2).toString();
			amountStr = numParts[0] + "." + twoDecimalsStr.split("\\.")[1];
		} catch (Exception e) { //Integer or Less than 3 decimals
			amountStr = amount;
		}
		
		return amountStr;
	}
	
	/**
     * Find the corresponding SKOS Concept from the provided FPM category.
     * 
     * @param String the FPM category
     * @return String the corresponding URI of the SKOS Concept
     */
	public String findFpmCategoryIndividual(String category) {
		
		String uri = "";
		
		if (category.equalsIgnoreCase("Tender")) {
			uri = Ontology.eLodPrefix + "Tender";
		} else if (category.equalsIgnoreCase("Contract")) {
			uri = Ontology.eLodPrefix + "Contract";
		} else if (category.equalsIgnoreCase("Payment")) {
			uri = Ontology.eLodPrefix + "Payment";
		} else if (category.equalsIgnoreCase("Budget")) {
			uri = Ontology.eLodPrefix + "Budget";
		} else if (category.equalsIgnoreCase("Committed")) {
			uri = Ontology.eLodPrefix + "Committed";
		} else if (category.equalsIgnoreCase("ExpenseApproval")) {
			uri = Ontology.eLodPrefix + "ExpenseApproval";
		} else if (category.equalsIgnoreCase("Assignment")) {
			uri = Ontology.eLodPrefix + "Assignment";
		} else if (category.equalsIgnoreCase("Notice")) {
			uri = Ontology.eLodPrefix + "Notice";
		} else if (category.equalsIgnoreCase("Award")) {
			uri = Ontology.eLodPrefix + "Award";
		}
		
		return uri;
	}
	
	/**
     * Keep the date from the provided dateTime Literal.
     * 
     * @param Literal the dateTime Literal
     * @return String the date from the dateTime Literal
     */
	public String getDateFromDateLiteral(Literal lastUpdateDate) {
		
		String date = lastUpdateDate.getString().split("T")[0];
		
		return date;
	}
	
	/**
     * Find the year from the provided dateTime Literal.
     * 
     * @param String the date filter
     * @return String the year of the dateTime Literal
     */
	public String getYearFromDateFilter(String dateFilter) {
		
		String year = dateFilter.split("-")[0];
		
		return year;
	}
	
	/**
     * Find the unique id of the Organization.
     * 
     * @param String the String representation of the URI of the Organization
     * @return String unique id of the Organization
     */
	public String findOrganizationId(String orgUri) {
		
		String organizationId = null;
		
		if (orgUri.contains("Organization/")) {
			organizationId = orgUri.split("Organization/")[1];
		} else if (orgUri.contains("OrganizationalUnit/")) {
			organizationId = orgUri.split("OrganizationalUnit/")[1];
		} else if (orgUri.contains("Person/")) {
			organizationId = orgUri.split("Person/")[1];
		} else {
			organizationId = orgUri.split("http://linkedeconomy.org/")[1];
		}
		
		return organizationId;
	}
	
	/**
     * Find and transform the current date into the yyyy-MM-dd format.
     * 
     * @return String the current date in the yyyy-MM-dd format
     */
	public String getCurrentDate() {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = sdf.format(cal.getTime());
		
		return currentDate;
	}
	
	/**
     * Calculate the Percentage change of the provided amounts.
     * 
     * @param float the initial amount
     * @param float the final amount
     * @return float the Percentage change of the provided amounts
     */
	public float calculatePercentageVariation(float previousAmount, float currentAmount) {
		
		float percentage = 0;
		
		if ( (previousAmount == 0) && (currentAmount == 0) ) {
			percentage = 0;
		} else if (previousAmount == 0) {
			percentage = Float.POSITIVE_INFINITY;
		} else {
			percentage = ( (currentAmount - previousAmount) / previousAmount) * 100;
			percentage = Math.round(percentage * 10); //for rounding purposes
			percentage = percentage / 10; //for rounding purposes
		}
		
		return percentage;
	}
	
	/**
     * Calculate the difference between the initial and the final counter.
     * 
     * @param int the initial counter
     * @param int the final counter
     * @return int the difference between the initial and the final counter
     */
	public int calculateCounterVariation(int previousCounter, int currentCounter) {
		
		int	diff =  currentCounter - previousCounter;
		
		return diff;
	}
	
	/**
     * Calculate the difference between the initial and the final rank.
     * 
     * @param int the initial rank
     * @param int the final rank
     * @return int the difference between the initial and the final rank
     */
	public int calculateRankVariation(int previousRank, int currentRank) {
		
		if ( (previousRank == -1) || (currentRank == -1) ) {
			return Integer.MIN_VALUE; //-999999999; //diff not available
		}
		
		int	diff =  previousRank - currentRank;
		
		return diff;
	}
	
	public void writeProblematicURIs(String fileName, String uri) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(uri);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	/**
     * Check whether the provided String Arrays contain at least a null element.
     * 
     * @param String[] the String Arrays to check
     * @return boolean whether the provided String Arrays contain at least a null element
     */
	public boolean checkStringArraysForNull(String[] categoryDtlsPrevious, String[] categoryDtlsCurrent) {
		
		for (String element : categoryDtlsPrevious) {
			if (element == null) {
				return true;
			} else {
				break;
			}
		}
		
		for (String element : categoryDtlsCurrent) {
			if (element == null) {
				return true;
			} else {
				break;
			}
		}
		
		return false;
	}

}