package automation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.concurrent.TimeUnit;

import utils.Logger;
import utils.QueryConfiguration;

/**
 * @author G. Razis
 */
public class AutoUploadMethods {
	
	/**
     * Handle the upload and checking process of the 
     * RDF file to the provided directory.
     * 
     * @param String the agent's category
     * @param String the name of the RDF file
     * 
     * @throws IOException
	 * @throws InterruptedException 
     */
	public void uploadStatisticsRdf(String category, String rdfFileName) throws IOException, InterruptedException {
		
		//move Statistics RDF file
		Path source = Paths.get(QueryConfiguration.SOURCE + rdfFileName);
		Path destination = Paths.get(QueryConfiguration.DESTINATION + File.separator);
		
		Files.move(source, destination.resolve(rdfFileName));
		Logger.getInstance().write(AutomatedStatistics.logFilename, "Moved file: " + rdfFileName);
		
		//upload RDF file
		insertModel(QueryConfiguration.DESTINATION, rdfFileName, rdfFileName.substring(0, rdfFileName.length() - 4));
		
		//check if upload is completed
		while ( !isInsertComplete(QueryConfiguration.DESTINATION, rdfFileName, rdfFileName.substring(0, rdfFileName.length() - 4)) ) {
			Logger.getInstance().write(AutomatedStatistics.logFilename, "Sleeping for " + QueryConfiguration.SLEEP + " minutes...");
			TimeUnit.MINUTES.sleep(QueryConfiguration.SLEEP);
			Logger.getInstance().write(AutomatedStatistics.logFilename, "Checking if Insert of " + category + " is completed");
		}
		
	}

	/**
     * Upload the RDF file of the provided directory.
     * 
     * @param String the directory of the RDF file
     * @param String the name of the RDF file
     * @param String the name of the generated script for the upload process
     * 
     * @throws IOException
	 * @throws InterruptedException 
     */
	private void insertModel(String rdfDircetory, String rdfFile, String scriptName) throws IOException, InterruptedException {
		
		String executorName = "execute_" + scriptName;
		String insertModelScript = "ld_dir('" + rdfDircetory + "', '" + rdfFile + "', '" + 
								   QueryConfiguration.queryGraphDiavgeiaIIStats + "');\nrdf_loader_run();";

		writeScript(rdfDircetory, scriptName, insertModelScript);
		writeScriptExecutor(rdfDircetory, executorName, scriptName);
		
		Logger.getInstance().write(AutomatedStatistics.logFilename, "For stability reasons sleep for 1' before uploading file...");
		TimeUnit.MINUTES.sleep(1);
		
		runScript(rdfDircetory, executorName);
		Logger.getInstance().write(AutomatedStatistics.logFilename, "Started uploading RDF...");
		
		Logger.getInstance().write(AutomatedStatistics.logFilename, "For stability reasons sleep for 1' after uploading file...");
		TimeUnit.MINUTES.sleep(1);
		
		//delete script files
		new File(rdfDircetory + File.separator + scriptName).delete();
		new File(rdfDircetory + File.separator + executorName).delete();
		
	}
	
	/**
     * Write a unix script and make it runnable.
     * 
     * @param String the directory of the script
     * @param String the name of the script
     * @param String the script
     * 
     * @throws IOException
     */
	private void writeScript(String folder, String filename, String script) throws IOException {
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(folder + File.separator + filename));
		writer.write(script);
		writer.close();
		Runtime.getRuntime().exec("chmod +x " + filename, null, new File(folder));
	}
	
	/**
     * Write a unix script which executes another script.
     * 
     * @param String the directory of the script
     * @param String the name of the script
     * @param String the script to be executed
     * 
     * @throws IOException
     */
	private void writeScriptExecutor(String folder, String filename, String scriptToExecute) throws IOException {
		
		String script = "isql-vt -H 127.0.0.1 -S 1111 -U "
					  + QueryConfiguration.username + " -P " 
					  + QueryConfiguration.password + " < " + scriptToExecute + "\n";

		writeScript(folder, filename, script);
	}
	
	/**
     * Execute the provided unix script.
     * 
     * @param String the directory of the script
     * @param String the name of the script
     * 
     * @throws IOException
     */
	private Process runScript (String folder, String filename) throws IOException {
		
		return Runtime.getRuntime().exec("./" + filename, null, new File(folder));
		
	}
	
	/**
     * Check whether the upload of the RDF file to the OLV is completed.
     * 
     * @param String the directory of the RDF file
     * @param String the name of the RDF file
     * @param String the name of the script which performs the check
     * 
     * @throws IOException
     * @throws InterruptedException
     */
	private boolean isInsertComplete(String rdfDircetory, String rdfFile, String scriptName) throws IOException, InterruptedException {
		
		String executorName = "execute_" + scriptName;
		String insertModelScript = "SELECT COUNT(*) FROM DB.DBA.load_list WHERE ll_graph='" + 
								   QueryConfiguration.queryGraphDiavgeiaIIStats + "' AND ll_state<>2;\n";

		writeScript(rdfDircetory, scriptName, insertModelScript);
		writeScriptExecutor(rdfDircetory, executorName, scriptName);

		boolean completed = checkDB(rdfDircetory, executorName);
		
		if (completed) {
			new File(rdfDircetory + File.separator + scriptName).delete();
			new File(rdfDircetory + File.separator + executorName).delete();
		}

		return completed;
	}
	
	/**
     * Query the OLV for pending uploads.
     * 
     * @param String the directory of the script
     * @param String the name of the script to be executed
     * 
     * @throws IOException
     */
	private boolean checkDB (String folder, String command) throws IOException, InterruptedException {
		
		Process p = runScript(folder, command);
		p.waitFor();

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String inputStr = "";
		String line = "";
		while ((line = reader.readLine()) != null) {
			inputStr += line + "\n";
		}
		reader.close();
		
		int integerIndex = inputStr.indexOf("INTEGER");
		int rowsIndex = inputStr.indexOf("1 Rows");
		inputStr = inputStr.substring(integerIndex, rowsIndex);
		inputStr = inputStr.replaceAll("[^0-9]", "");
		boolean done = inputStr.equals("0");
		if (done) {
			Logger.getInstance().write(AutomatedStatistics.logFilename, "Insert Completed!");
		}
		
		return done;
	}
	
	/**
     * Create an informative file upon completion.
     * 
     * @param String the agent's category
     * 
     * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
     */
	public void createCompletionFile(String category) throws FileNotFoundException, UnsupportedEncodingException {
		
		PrintWriter writer = new PrintWriter(QueryConfiguration.SOURCE + "start" + category + "Json.txt", "UTF-8");
		writer.close();
		Logger.getInstance().write(AutomatedStatistics.logFilename, "Created 'start" + category + "Json.txt' file");
		
	}
	
}