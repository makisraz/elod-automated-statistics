package automation;

import java.io.File;

import java.util.concurrent.TimeUnit;

import statisticsDiavgeiaII.AcrMain;
import statisticsDiavgeiaII.VariationMain;

import utils.DateMethods;
import utils.Logger;
import utils.QueryConfiguration;

/**
 * @author G. Razis
 */
public class AutomatedStatistics {
	
	public static final String logFilename = "autoUploaderStatsLog";

	/**
	 * If proper informative file from Diavgeia exists
	 * 1) Calculate ACR Statistics (B, S, H) and create RDF files
	 * 2) Upload the ACR RDF files
	 * 
	 * When finished ACR RDF uploading
	 * 3) Calculate V Statistics (B, S, H) and create RDF files
	 * 4) Upload the V RDF files
	 * 5) Create an informative txt file
	 * 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		
		for (;;) {
			
			//define the agent's category
			String category = getAgentCategory();
			
			if (initiateStatistics(category)) {
				
				AutoUploadMethods upl = new AutoUploadMethods();
				
				//define the graph (changes every year)
				QueryConfiguration.queryGraphDiavgeiaII += "/" + DateMethods.getMonthsYear();
				
				//set the name of the ACR RDF file
				QueryConfiguration.rdfName = "StatisticsDiavgeiaII" + category + DateMethods.getMonthsYear() + "_" + 
											 DateMethods.getPreviousMonth(-1) + ".rdf";
				
				//Step 1: calculate ACR Statistics
				Logger.getInstance().write(logFilename, "Creating ACR Statistics...");
				AcrMain.main(args);
				Logger.getInstance().write(logFilename, "Created ACR Statistics!");
				
				//Step 2: upload ACR Statistics
				try {
					upl.uploadStatisticsRdf(category, QueryConfiguration.rdfName);
				} catch (Exception e) {
					Logger.getInstance().error(logFilename, e);
				}
				
				//create the name of the V RDF file
				QueryConfiguration.rdfName = "StatisticsDiavgeiaII" + "Variation" + category + DateMethods.getMonthsYear() + "_" + 
											 DateMethods.getPreviousMonth(-2) + "-" + DateMethods.getPreviousMonth(-1) + ".rdf";
				
				//Step 3: calculate V Statistics
				Logger.getInstance().write(logFilename, "Creating V Statistics...");
				VariationMain.main(args);
				Logger.getInstance().write(logFilename, "Created V Statistics!");
				
				//Step 4: upload V Statistics
				try {
					upl.uploadStatisticsRdf(category, QueryConfiguration.rdfName);
				} catch (Exception e) {
					Logger.getInstance().error(logFilename, e);
				}
				
				//Step 5: create informative txt file
				try {
					upl.createCompletionFile(category);
				} catch (Exception e) {
					Logger.getInstance().error(logFilename, e);
				}
				
				//Step 6: delete informative 'startCATEGORYStatistics.txt' file
				new File(QueryConfiguration.SOURCE + File.separator + "start" + category + "Statistics.txt").delete();
				Logger.getInstance().write(logFilename, "Deleted 'start" + category + "Statistics.txt' file");
				Logger.getInstance().write(logFilename, "Finished!\n");
				break;
			} else {
				Logger.getInstance().write(logFilename, "Upload of RDF is still pending...");
			}
		}

	}
	
	private static boolean initiateStatistics(String agentCategory) throws InterruptedException {
		
		boolean initiate = false;
		
		if ( new File(QueryConfiguration.SOURCE + File.separator + "start" + agentCategory + "Statistics.txt").exists() ) {
			initiate = true;
		} else {
			Logger.getInstance().write(logFilename, "Sleeping for " + QueryConfiguration.SLEEP + " minutes...");
			TimeUnit.MINUTES.sleep(QueryConfiguration.SLEEP);
		}
		
		return initiate;
	}
	
	private static String getAgentCategory() {
		
		String category = null;
		
		if (QueryConfiguration.buyers) {
			category = "Buyers";
		} else if (QueryConfiguration.sellers) {
			category = "Sellers";
		} else if (QueryConfiguration.hybrids) {
			category = "Hybrids";
		}
		
		return category;
	}
	
}