package statisticsDiavgeiaII;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.math.BigDecimal;

import objects.StatisticAcrDtls;
import objects.StatisticVariationDtls;

import ontology.Ontology;
import ontology.OntologyInitialization;

import utils.HelperMethods;
import utils.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class RdfActions {
	
	/**
     * Fetch the details regarding the statistics of the Organization, 
     * create the RDF triples and add them to the (existing) model.
     * 
     * @param StatisticAcrDtls the statistical details of an Organization
     * @param Model the model to add the triples
     */
	public void createAcrStatistics(StatisticAcrDtls stats, Model model, Literal lastUpdateDate, String buyerOrSeller, String currentDate) {
		
		HelperMethods hm = new HelperMethods();
		
		Resource statisticResource = null;
		Resource aggregateResource = null;
		Resource counterResource = null;
		Resource rankResource = null;
		
		String orgId = hm.findOrganizationId(stats.getOrganization().toString()); //AFM or AFM/ID
		String dateUpdated = hm.getDateFromDateLiteral(lastUpdateDate); //last date of the data
		String categoryFpmIndividualUri = hm.findFpmCategoryIndividual(stats.getCategory());
		String referenceTime = QueryConfiguration.dateFilterFrom; //hm.getYearFromDateFilter(QueryConfiguration.dateFilterFrom)
		
		/* Step 1: Create Resources and bind their properties */
    	/** statisticResource **/
		statisticResource = model.createResource(Ontology.instancePrefix + "Statistic/" + orgId + "/" + buyerOrSeller + "/" + dateUpdated + "/" + referenceTime + "/" + currentDate, Ontology.statisticResource);
		statisticResource.addLiteral(Ontology.referenceTime, model.createTypedLiteral(referenceTime, XSDDatatype.XSDdate)); //XSDDatatype depending on 'time'
		statisticResource.addLiteral(Ontology.referenceTimeDuration, model.createTypedLiteral("P1M", XSDDatatype.XSDduration));
		statisticResource.addLiteral(Ontology.lastUpdateDate, lastUpdateDate);
		statisticResource.addProperty(Ontology.orgActivity, model.createTypedLiteral(buyerOrSeller, XSDDatatype.XSDstring));
		statisticResource.addProperty(Ontology.calculationDate, model.createTypedLiteral(currentDate, XSDDatatype.XSDdate));
		
		/** aggregateResource **/
		aggregateResource = model.createResource(Ontology.instancePrefix + "Aggregate/" + orgId + "/" + buyerOrSeller + "/" + stats.getCategory() + "/" + dateUpdated + "/" + referenceTime + "/" + currentDate, Ontology.aggregateResource);
		aggregateResource.addLiteral(Ontology.aggregatedAmount, model.createTypedLiteral(hm.roundAggregatedAmount(new BigDecimal(stats.getAggregatedAmount()).toPlainString()), XSDDatatype.XSDfloat));
		aggregateResource.addProperty(Ontology.hasCategory, model.getResource(categoryFpmIndividualUri));
		
		/** counterResource **/
		counterResource = model.createResource(Ontology.instancePrefix + "Counter/" + orgId + "/" + buyerOrSeller + "/" + stats.getCategory() + "/" + dateUpdated + "/" + referenceTime + "/" + currentDate, Ontology.counterResource);
		counterResource.addLiteral(Ontology.counter, model.createTypedLiteral(stats.getCounter(), XSDDatatype.XSDint));
		counterResource.addProperty(Ontology.hasCategory, model.getResource(categoryFpmIndividualUri));
		
		/** rankResource **/
		rankResource = model.createResource(Ontology.instancePrefix + "Rank/" + orgId + "/" + buyerOrSeller + "/" + stats.getCategory() + "/" + dateUpdated + "/" + referenceTime + "/" + currentDate, Ontology.rankResource);
		rankResource.addLiteral(Ontology.rank, model.createTypedLiteral(stats.getRank(), XSDDatatype.XSDint));
		rankResource.addProperty(Ontology.hasCategory, model.getResource(categoryFpmIndividualUri));
		
		/* Step 2: Connect the Resources */
		/** Statistic - Organization **/
		statisticResource.addProperty(Ontology.isStatisticOf, stats.getOrganization());
		
		/** Statistic - Aggregate **/
		statisticResource.addProperty(Ontology.hasAggregate, aggregateResource);
		
		/** Statistic - Counter **/
		statisticResource.addProperty(Ontology.hasCounter, counterResource);
		
		/** Statistic - Rank **/
		statisticResource.addProperty(Ontology.hasRank, rankResource);
	}
	
	/**
     * Associate the Variations of the Aggregate, Counter and Rank with the respective 
     * classes, create the RDF triples and add them to the (existing) model.
     * 
     * @param StatisticVariationDtls the the Variations of the Aggregate, Counter and Rank of an Organization
     * @param Model the model to add the triples
     */
	public void createVariationStatistics(StatisticVariationDtls stats, Model model) {
		
		/** aggregateResource **/
		Resource aggregateResource = model.createResource(stats.getAggregateAmountUri(), Ontology.aggregateResource);
		aggregateResource.addProperty(Ontology.variation, model.createTypedLiteral(stats.getVariationAggregatedAmount(), XSDDatatype.XSDfloat));
		
		/** counterResource **/
		Resource counterResource = model.createResource(stats.getCounterUri(), Ontology.counterResource);
		counterResource.addProperty(Ontology.variation, model.createTypedLiteral(stats.getVariationCounter(), XSDDatatype.XSDint));
		
		/** rankResource **/
		Resource rankResource = model.createResource(stats.getRankUri(), Ontology.rankResource);
		rankResource.addProperty(Ontology.variation, model.createTypedLiteral(stats.getVariationRank(), XSDDatatype.XSDint));
	}
	
	/**
     * Fetch the existing model depending whether it is located remotely or locally.
     * 
     * @param boolean is the model located remotely or locally?
     * @return Model the new model
     */
	public Model remoteOrLocalModel (boolean isRemote) {
		
		OntologyInitialization ontInit = new OntologyInitialization();
		
		Model remoteModel = ModelFactory.createDefaultModel();
		
		if (isRemote) {
			VirtGraph graph = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII, QueryConfiguration.connectionString, 
											QueryConfiguration.username, QueryConfiguration.password);
			System.out.println("\nLoaded Diavgeia II Graph!\n");
	    	remoteModel = new VirtModel(graph);
		} else {
			try {
				InputStream is = new FileInputStream(QueryConfiguration.SOURCE + "/" + QueryConfiguration.rdfName);
				remoteModel.read(is,null);
				is.close();
			} catch (Exception e) { //empty file
			}
		}
		
		ontInit.setPrefixes(remoteModel);
		
		return remoteModel;
		
	}
	
	/**
     * Store the Model.
     * 
     * @param Model the model
     */
	public void writeModel(Model model) {
		
		try {
			System.out.println("\nSaving Model...");
			FileOutputStream fos = new FileOutputStream(QueryConfiguration.SOURCE + "/" + QueryConfiguration.rdfName);
			model.write(fos, "RDF/XML-ABBREV", QueryConfiguration.SOURCE + "/" + QueryConfiguration.rdfName);
			fos.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}