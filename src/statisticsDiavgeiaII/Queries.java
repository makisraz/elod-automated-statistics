package statisticsDiavgeiaII;

import java.util.ArrayList;

import utils.HelperMethods;
import utils.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class Queries {
	
	private HelperMethods hm = new HelperMethods();
	
	/** queries regarding the Buyers **/
	/* ACR */
	public ArrayList<Resource> getAllBuyers(VirtGraph graphDiavgeiaII) {
		
		ArrayList<Resource> buyersList = new ArrayList<Resource>();
		
		String queryForeis = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					 "PREFIX dcterms: <http://purl.org/dc/terms/> " +
					 "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					 "PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					 "PREFIX org: <http://www.w3.org/ns/org#> " +
					 "SELECT DISTINCT ?org " +
					 "FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					 "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					 "WHERE { " +
						 "{ " +
						 "?decision dc:publisher ?unit ; " +
						           "dcterms:issued ?date . " +
						 "?org org:hasUnit ?unit . " +
						 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
						 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
						 "FILTER NOT EXISTS {?decision elod:hasCorrectedDecision ?correctedDecision} . " +
						 "FILTER EXISTS {?org rdf:type gr:BusinessEntity} . " +
						 "} " +
					 "UNION " +
						 "{ " +
						 "?decision dc:publisher ?unit ; " +
						           "dcterms:issued ?date . " +
						 "?unit rdfs:seeAlso ?org . " +
						 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
						 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
						 "FILTER NOT EXISTS {?decision elod:hasCorrectedDecision ?correctedDecision} . " +
						 "} " +
					 "}";
	
		VirtuosoQueryExecution vqeForeis = VirtuosoQueryExecutionFactory.create(queryForeis, graphDiavgeiaII);
		ResultSet resultsForeis = vqeForeis.execSelect();
		
		while (resultsForeis.hasNext()) {
			QuerySolution result = resultsForeis.nextSolution();
			if ( !((result.getResource("org").getURI().contains("\"")) || 
					(result.getResource("org").getURI().contains("`")) || 
					(result.getResource("org").getURI().contains(" ")) || 
					(result.getResource("org").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) || 
					(result.getResource("org").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/OrganizationalUnit/"))) ) {
				buyersList.add(result.getResource("org"));
			}
		}
		
		vqeForeis.close();
		
		return buyersList;
	}
	
	public String[] getBuyerCommittedDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerCommittedDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerCommittedDtls = getBuyerCommittedDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerCommittedDtls = getBuyerCommittedDtlsUnit(graphDiavgeiaII, unitUri);
		}
		
		return buyerCommittedDtls;
	}
	
	private String[] getBuyerCommittedDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalCommitted = null;
		String totalAmount = null;
		
		String queryCommitted = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?committedItem) AS ?committedItemCounter) (SUM(xsd:decimal(?amount)) AS ?committedItemAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?committedItem elod:hasExpenditureLine ?expLine ; " +
					               "dc:publisher ?unit ; " +
					               "dcterms:issued ?date ; " +
					               "elod:isRecalledExpenseDecision ?recall ; " +
					               "rdf:type elod:CommittedItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + "> org:hasUnit ?unit . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom +"T00:00:00Z\"^^xsd:dateTime) . " +
					"FILTER (?date < \"" + QueryConfiguration.dateFilterTo +"T00:00:00Z\"^^xsd:dateTime) . " +
					"FILTER NOT EXISTS {?committedItem elod:hasCorrectedDecision ?correctedDecision} . " +
					"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?recall = 0) . " +
					"}";

		VirtuosoQueryExecution vqeCommitted = VirtuosoQueryExecutionFactory.create(queryCommitted, graphDiavgeiaII);
		ResultSet resultsCommitted = vqeCommitted.execSelect();
		
		if (resultsCommitted.hasNext()) {
			QuerySolution result = resultsCommitted.nextSolution();
			totalCommitted = result.getLiteral("committedItemCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("committedItemAmount"));
		}
		
		vqeCommitted.close();
		
		return new String[]{totalCommitted, totalAmount};
	}
	
	private String[] getBuyerCommittedDtlsUnit(VirtGraph graphDiavgeiaII, String unitUri) {
		
		String totalCommitted = null;
		String totalAmountStr = null;
		
		String queryCommitted = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?committedItem) AS ?committedItemCounter) (SUM(xsd:decimal(?amount)) AS ?committedItemAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?committedItem elod:hasExpenditureLine ?expLine ; " +
					               "dc:publisher <" + unitUri + "> ; " +
					               "dcterms:issued ?date ; " +
					               "elod:isRecalledExpenseDecision ?recall ; " +
					               "rdf:type elod:CommittedItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom +"T00:00:00Z\"^^xsd:dateTime) . " +
					"FILTER (?date < \"" + QueryConfiguration.dateFilterTo +"T00:00:00Z\"^^xsd:dateTime) . " +
					"FILTER NOT EXISTS {?committedItem elod:hasCorrectedDecision ?correctedDecision} . " +
					"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?recall = 0) . " +
					"}";

		VirtuosoQueryExecution vqeCommitted = VirtuosoQueryExecutionFactory.create(queryCommitted, graphDiavgeiaII);
		ResultSet resultsCommitted = vqeCommitted.execSelect();
		
		if (resultsCommitted.hasNext()) {
			QuerySolution result = resultsCommitted.nextSolution();
			totalCommitted = result.getLiteral("committedItemCounter").getString();
			totalAmountStr = hm.roundAmount(result.getLiteral("committedItemAmount"));
		}
		
		vqeCommitted.close();
		
		return new String[]{totalCommitted, totalAmountStr};
	}
	
	public String[] getBuyerExpenseApprovalDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerExpenseApprovalDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerExpenseApprovalDtls = getBuyerExpenseApprovalDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerExpenseApprovalDtls = getBuyerExpenseApprovalDtlsUnitOrg(graphDiavgeiaII, buyerUri);
		}
		
		return buyerExpenseApprovalDtls;
	}
	
	private String[] getBuyerExpenseApprovalDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalExpenseApprovals = null;
		String totalAmount = null;
		
		String queryExpenseApproval = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?expenseApproval) AS ?expenseApprovalCounter) (SUM(xsd:decimal(?amount)) AS ?expenseApprovalAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?expenseApproval elod:hasExpenditureLine ?expLine ; " +
									 "dc:publisher ?unit ; " +
					                 "dcterms:issued ?date ; " +
					                 "rdf:type elod:ExpenseApprovalItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + "> org:hasUnit ?unit . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?expenseApproval elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqeExpenseApproval = VirtuosoQueryExecutionFactory.create(queryExpenseApproval, graphDiavgeiaII);
		ResultSet resultsExpenseApproval = vqeExpenseApproval.execSelect();	
		
		if (resultsExpenseApproval.hasNext()) {
			QuerySolution result = resultsExpenseApproval.nextSolution();
			totalExpenseApprovals = result.getLiteral("expenseApprovalCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("expenseApprovalAmount"));
		}
		
		vqeExpenseApproval.close();
		
		return new String[]{totalExpenseApprovals, totalAmount};
	}
	
	private String[] getBuyerExpenseApprovalDtlsUnitOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalExpenseApprovals = null;
		String totalAmount = null;
		
		String queryExpenseApproval = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?expenseApproval) AS ?expenseApprovalCounter) (SUM(xsd:decimal(?amount)) AS ?expenseApprovalAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?expenseApproval elod:hasExpenditureLine ?expLine ; " +
									 "elod:buyer <" + buyerUri + "> ; " +
					                 "dcterms:issued ?date ; " +
					                 "rdf:type elod:ExpenseApprovalItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?expenseApproval elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqeExpenseApproval = VirtuosoQueryExecutionFactory.create(queryExpenseApproval, graphDiavgeiaII);
		ResultSet resultsExpenseApproval = vqeExpenseApproval.execSelect();	
		
		if (resultsExpenseApproval.hasNext()) {
			QuerySolution result = resultsExpenseApproval.nextSolution();
			totalExpenseApprovals = result.getLiteral("expenseApprovalCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("expenseApprovalAmount"));
		}
		
		vqeExpenseApproval.close();
		
		return new String[]{totalExpenseApprovals, totalAmount};
	}
	
	public String[] getBuyerPaymentDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerPaymentDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerPaymentDtls = getBuyerPaymentDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerPaymentDtls = getBuyerPaymentDtlsUnitOrg(graphDiavgeiaII, buyerUri);
		}
		
		return buyerPaymentDtls;
	}
	
	private String[] getBuyerPaymentDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalPayments = null;
		String totalAmount = null;
		
		String queryPayments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?spendingItem) AS ?paymentsCounter) (SUM(xsd:decimal(?amount)) AS ?paymentAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?spendingItem elod:hasExpenditureLine ?expLine ; " +
								  "dc:publisher ?unit ; " +
					              "dcterms:issued ?date ; " +
					              "rdf:type elod:SpendingItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + "> org:hasUnit ?unit . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?spendingItem elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqePayments = VirtuosoQueryExecutionFactory.create(queryPayments, graphDiavgeiaII);
		ResultSet resultsPayments = vqePayments.execSelect();		
		
		if (resultsPayments.hasNext()) {
			QuerySolution result = resultsPayments.nextSolution();
			totalPayments = result.getLiteral("paymentsCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("paymentAmount"));
		}
		
		vqePayments.close();
		
		return new String[]{totalPayments, totalAmount};
	}
	
	private String[] getBuyerPaymentDtlsUnitOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalPayments = null;
		String totalAmount = null;
		
		String queryPayments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?spendingItem) AS ?paymentsCounter) (SUM(xsd:decimal(?amount)) AS ?paymentAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?spendingItem elod:hasExpenditureLine ?expLine ; " +
								  "elod:buyer <" + buyerUri + "> ; " +
					              "dcterms:issued ?date ; " +
					              "rdf:type elod:SpendingItem . " +
					"?expLine elod:amount ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?spendingItem elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqePayments = VirtuosoQueryExecutionFactory.create(queryPayments, graphDiavgeiaII);
		ResultSet resultsPayments = vqePayments.execSelect();		
		
		if (resultsPayments.hasNext()) {
			QuerySolution result = resultsPayments.nextSolution();
			totalPayments = result.getLiteral("paymentsCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("paymentAmount"));
		}
		
		vqePayments.close();
		
		return new String[]{totalPayments, totalAmount};
	}
	
	public String[] getBuyerAssignmentDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerAssignmentDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerAssignmentDtls = getBuyerAssignmentDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerAssignmentDtls = getBuyerAssignmentDtlsUnit(graphDiavgeiaII, unitUri);
		}
		
		return buyerAssignmentDtls;
	}
	
	public String[] getBuyerAssignmentDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalAssignments = null;
		String totalAmount = null;
		
		String queryAssignments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?contract) AS ?contractD1Counter) (SUM(xsd:decimal(?amount)) AS ?contractD1Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "dc:publisher ?unit ; " +
					          "dcterms:issued ?date ; " +
					          "pc:agreedPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + ">  org:hasUnit ?unit . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.1\"^^xsd:string) . " +
					"}";

		VirtuosoQueryExecution vqeAssignments = VirtuosoQueryExecutionFactory.create(queryAssignments, graphDiavgeiaII);
		ResultSet resultsAssignments = vqeAssignments.execSelect();	
		
		if (resultsAssignments.hasNext()) {
			QuerySolution result = resultsAssignments.nextSolution();
			totalAssignments = result.getLiteral("contractD1Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD1Amount"));
		}
		
		vqeAssignments.close();
		
		return new String[]{totalAssignments, totalAmount};
	}
	
	private String[] getBuyerAssignmentDtlsUnit(VirtGraph graphDiavgeiaII, String unitUri) {
		
		String totalAssignments = null;
		String totalAmount = null;
		
		String queryAssignments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?contract) AS ?contractD1Counter) (SUM(xsd:decimal(?amount)) AS ?contractD1Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "dc:publisher <" + unitUri + "> ; " +
					          "dcterms:issued ?date ; " +
					          "pc:agreedPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.1\"^^xsd:string) . " +
					"}";

		VirtuosoQueryExecution vqeAssignments = VirtuosoQueryExecutionFactory.create(queryAssignments, graphDiavgeiaII);
		ResultSet resultsAssignments = vqeAssignments.execSelect();	
		
		if (resultsAssignments.hasNext()) {
			QuerySolution result = resultsAssignments.nextSolution();
			totalAssignments = result.getLiteral("contractD1Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD1Amount"));
		}
		
		vqeAssignments.close();
		
		return new String[]{totalAssignments, totalAmount};
	}
	
	public String[] getBuyerNoticeDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerNoticeDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerNoticeDtls = getBuyerNoticeDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerNoticeDtls = getBuyerNoticeDtlsUnit(graphDiavgeiaII, unitUri);
		}
		
		return buyerNoticeDtls;
	}
	
	private String[] getBuyerNoticeDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalNotices = null;
		String totalAmount = null;
		
		String queryNotices = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?contract) AS ?contractD21Counter) (SUM(xsd:decimal(?amount)) AS ?contractD21Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "dc:publisher ?unit ; " +
					          "dcterms:issued ?date ; " +
					          "pc:documentsPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + "> org:hasUnit ?unit . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.2.1\"^^xsd:string) . " +
					"}";

		VirtuosoQueryExecution vqeNotices = VirtuosoQueryExecutionFactory.create(queryNotices, graphDiavgeiaII);
		ResultSet resultsNotices = vqeNotices.execSelect();	
		
		if (resultsNotices.hasNext()) {
			QuerySolution result = resultsNotices.nextSolution();
			totalNotices = result.getLiteral("contractD21Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD21Amount"));
		}
		
		vqeNotices.close();
		
		return new String[]{totalNotices, totalAmount};
	}
	
	private String[] getBuyerNoticeDtlsUnit(VirtGraph graphDiavgeiaII, String unitUri) {
		
		String totalNotices = null;
		String totalAmount = null;
		
		String queryNotices = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?contract) AS ?contractD21Counter) (SUM(xsd:decimal(?amount)) AS ?contractD21Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "elod:buyer <" + unitUri + "> ; " +
					          "dcterms:issued ?date ; " +
					          "pc:documentsPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.2.1\"^^xsd:string) . " +
					"}";

		VirtuosoQueryExecution vqeNotices = VirtuosoQueryExecutionFactory.create(queryNotices, graphDiavgeiaII);
		ResultSet resultsNotices = vqeNotices.execSelect();	
		
		if (resultsNotices.hasNext()) {
			QuerySolution result = resultsNotices.nextSolution();
			totalNotices = result.getLiteral("contractD21Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD21Amount"));
		}
		
		vqeNotices.close();
		
		return new String[]{totalNotices, totalAmount};
	}
	
	public String[] getBuyerAwardDtls(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String[] buyerAwardDtls = null;
		
		String unitUri = checkForUnit(graphDiavgeiaII, buyerUri);
		
		if (unitUri == null) { //supervisor
			buyerAwardDtls = getBuyerAwardDtlsSuperOrg(graphDiavgeiaII, buyerUri);
		} else { //unit with Vat Id
			buyerAwardDtls = getBuyerAwardDtlsUnit(graphDiavgeiaII, unitUri);
		}
		
		return buyerAwardDtls;
	}
	
	private String[] getBuyerAwardDtlsSuperOrg(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String totalAwards = null;
		String totalAmount = null;
		
		String queryAwards = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"PREFIX org: <http://www.w3.org/ns/org#> " +
					"SELECT (COUNT(?contract) AS ?contractD22Counter) (SUM(xsd:decimal(?amount)) AS ?contractD22Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "dc:publisher ?unit ; " +
					          "dcterms:issued ?date ; " +
					          "pc:actualPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"<" + buyerUri + "> org:hasUnit ?unit . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.2.2\"^^xsd:string) . " +
					"}";
	
		VirtuosoQueryExecution vqeAwards = VirtuosoQueryExecutionFactory.create(queryAwards, graphDiavgeiaII);
		ResultSet resultsAwards = vqeAwards.execSelect();	
		
		if (resultsAwards.hasNext()) {
			QuerySolution result = resultsAwards.nextSolution();
			totalAwards = result.getLiteral("contractD22Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD22Amount"));
		}
		
		vqeAwards.close();
		
		return new String[]{totalAwards, totalAmount};
	}
	
	private String[] getBuyerAwardDtlsUnit(VirtGraph graphDiavgeiaII, String unitUri) {
		
		String totalAwards = null;
		String totalAmount = null;
		
		String queryAwards = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"SELECT (COUNT(?contract) AS ?contractD22Counter) (SUM(xsd:decimal(?amount)) AS ?contractD22Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "dc:publisher <" + unitUri + "> ; " +
					          "dcterms:issued ?date ; " +
					          "pc:actualPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
					"FILTER (?cntrTypeId = \"Δ.2.2\"^^xsd:string) . " +
					"}";
	
		VirtuosoQueryExecution vqeAwards = VirtuosoQueryExecutionFactory.create(queryAwards, graphDiavgeiaII);
		ResultSet resultsAwards = vqeAwards.execSelect();	
		
		if (resultsAwards.hasNext()) {
			QuerySolution result = resultsAwards.nextSolution();
			totalAwards = result.getLiteral("contractD22Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD22Amount"));
		}
		
		vqeAwards.close();
		
		return new String[]{totalAwards, totalAmount};
	}
	
	/** queries regarding the Sellers **/
	/* ACR */
	public ArrayList<Resource> getAllSellers(VirtGraph graphDiavgeiaII) {
		
		ArrayList<Resource> sellersList = new ArrayList<Resource>();
		
		//part 1: Contracts
		String queryAnadoxoi = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					 "PREFIX dcterms: <http://purl.org/dc/terms/> " +
					 "SELECT DISTINCT ?seller " +
					 "FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					 "WHERE { " +
					 "?contract elod:seller ?seller ; " +
					 		   "dcterms:issued ?date . " +
					 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			 "FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
					 "}";

		VirtuosoQueryExecution vqeAnadoxoi = VirtuosoQueryExecutionFactory.create(queryAnadoxoi, graphDiavgeiaII);
		ResultSet resultsAnadoxoi = vqeAnadoxoi.execSelect();
		
		while (resultsAnadoxoi.hasNext()) {
			QuerySolution result = resultsAnadoxoi.nextSolution();
			if ( !((result.getResource("seller").getURI().contains("\"")) || 
					(result.getResource("seller").getURI().contains("`")) || 
					(result.getResource("seller").getURI().contains(" ")) || 
					(result.getResource("seller").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) || 
					(result.getResource("seller").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Person/"))) ) {
				sellersList.add(result.getResource("seller"));
			}
		}
		
		//part 2: Decisions
		queryAnadoxoi = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				 "PREFIX dcterms: <http://purl.org/dc/terms/> " +
				 "SELECT DISTINCT ?seller " +
				 "FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
				 "WHERE { " +
				 "?expLine elod:seller ?seller . " +
				 "?decision elod:hasExpenditureLine ?expLine ; " +
				 		   "dcterms:issued ?date . " +
				 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
	  			 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
	  			 "FILTER NOT EXISTS {?decision elod:hasCorrectedDecision ?correctedDecision} . " +
				 "}";
		
		vqeAnadoxoi = VirtuosoQueryExecutionFactory.create(queryAnadoxoi, graphDiavgeiaII);
		resultsAnadoxoi = vqeAnadoxoi.execSelect();
		
		while (resultsAnadoxoi.hasNext()) {
			QuerySolution result = resultsAnadoxoi.nextSolution();
			if ( !((result.getResource("seller").getURI().contains("\"")) || 
					(result.getResource("seller").getURI().contains("`")) || 
					(result.getResource("seller").getURI().contains(" ")) || 
					(result.getResource("seller").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) || 
					(result.getResource("seller").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Person/"))) ) {
				if (!sellersList.contains(result.getResource("seller"))) { //do not add existing items
					sellersList.add(result.getResource("seller"));
				} else {
					System.out.println("exists");
				}
			}
		}
		
		vqeAnadoxoi.close();
		
		return sellersList;
	}
	
	public String[] getSellerExpenseApprovalsDtls(VirtGraph graphDiavgeiaII, String sellerUri) {
		
		String totalExpenseApprovals = null;
		String totalAmount = null;
		
		String queryExpenseApprovals = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?expenseApproval) AS ?expenseApprovalCounter) (SUM(xsd:decimal(?amount)) AS ?expenseApprovalAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"WHERE { " +
					"?expenseApproval elod:hasExpenditureLine ?expLine ; " +
					                 "dcterms:issued ?date ; " +
					                 "rdf:type elod:ExpenseApprovalItem . " +
					"?expLine elod:amount ?ups ; " +
					         "elod:seller <" + sellerUri + "> . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?expenseApproval elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqeExpenseApprovals = VirtuosoQueryExecutionFactory.create(queryExpenseApprovals, graphDiavgeiaII);
		ResultSet resultsExpenseApprovals = vqeExpenseApprovals.execSelect();
		
		if (resultsExpenseApprovals.hasNext()) {
			QuerySolution result = resultsExpenseApprovals.nextSolution();
			totalExpenseApprovals = result.getLiteral("expenseApprovalCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("expenseApprovalAmount"));
		}
		
		vqeExpenseApprovals.close();
		
		return new String[]{totalExpenseApprovals, totalAmount};
	}
	
	public String[] getSellerPaymentDtls(VirtGraph graphDiavgeiaII, String sellerUri) {
		
		String totalPayments = null;
		String totalAmount = null;
		
		String queryPayments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (COUNT(?spendingItem) AS ?paymentsCounter) (SUM(xsd:decimal(?amount)) AS ?paymentAmount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"WHERE { " +
					"?spendingItem elod:hasExpenditureLine ?expLine ; " +
					              "dcterms:issued ?date ; " +
					              "rdf:type elod:SpendingItem . " +
					"?expLine elod:amount ?ups ; " +
					         "elod:seller  <" + sellerUri + "> . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?spendingItem elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqePayments = VirtuosoQueryExecutionFactory.create(queryPayments, graphDiavgeiaII);
		ResultSet resultsPayments = vqePayments.execSelect();
		
		if (resultsPayments.hasNext()) {
			QuerySolution result = resultsPayments.nextSolution();
			totalPayments = result.getLiteral("paymentsCounter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("paymentAmount"));
		}
		
		vqePayments.close();
		
		return new String[]{totalPayments, totalAmount};
	}
	
	public String[] getSellerAssignmentDtls(VirtGraph graphDiavgeiaII, String sellerUri) {
		
		String totalAssignments = null;
		String totalAmount = null;
		
		String queryAssignments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"SELECT (COUNT(?contract) AS ?contractD1Counter) (SUM(xsd:decimal(?amount)) AS ?contractD1Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "elod:seller <" + sellerUri + "> ; " +
					          "dcterms:issued ?date ; " +
					          "pc:agreedPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";

		VirtuosoQueryExecution vqeAssignments = VirtuosoQueryExecutionFactory.create(queryAssignments, graphDiavgeiaII);
		ResultSet resultsAssignments = vqeAssignments.execSelect();
		
		if (resultsAssignments.hasNext()) {
			QuerySolution result = resultsAssignments.nextSolution();
			totalAssignments = result.getLiteral("contractD1Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD1Amount"));
		}
		
		vqeAssignments.close();
		
		return new String[]{totalAssignments, totalAmount};
	}

	public String[] getSellerAwardDtls(VirtGraph graphDiavgeiaII, String sellerUri) {
		
		String totalAwards = null;
		String totalAmount = null;
		
		String queryAwards = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcterms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"SELECT (COUNT(?contract) AS ?contractD22Counter) (SUM(xsd:decimal(?amount)) AS ?contractD22Amount) " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"WHERE { " +
					"?contract elod:decisionTypeId ?cntrTypeId ; " +
					          "elod:seller <" + sellerUri + "> ; " +
					          "dcterms:issued ?date ; " +
					          "pc:actualPrice ?ups . " +
					"?ups gr:hasCurrencyValue ?amount . " +
		  			"FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?contract elod:hasCorrectedDecision ?correctedDecision} . " +
		  			"FILTER NOT EXISTS {?ups elod:riskError \"1\"^^<http://www.w3.org/2001/XMLSchema#boolean>} . " +
		  			"}";
	
		VirtuosoQueryExecution vqeAwards = VirtuosoQueryExecutionFactory.create(queryAwards, graphDiavgeiaII);
		ResultSet resultsAwards = vqeAwards.execSelect();
		
		if (resultsAwards.hasNext()) {
			QuerySolution result = resultsAwards.nextSolution();
			totalAwards = result.getLiteral("contractD22Counter").getString();
			totalAmount = hm.roundAmount(result.getLiteral("contractD22Amount"));
		}
		
		vqeAwards.close();
		
		return new String[]{totalAwards, totalAmount};
	}
	
	/** queries regarding the Hybrids **/
	/* ACR */
	public ArrayList<Resource> getAllHybrids(VirtGraph graphDiavgeiaII) {
		
		ArrayList<Resource> hybridsList = new ArrayList<Resource>();
		
		String queryHybrids = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				 "PREFIX dcterms: <http://purl.org/dc/terms/> " +
				 "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				 "PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
				 "PREFIX org: <http://www.w3.org/ns/org#> " +
				 "SELECT DISTINCT ?hybridOrg " +
				 "FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
				 "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				 "WHERE { " +
					 "{ " +
					 "?decision1 dc:publisher ?unit ; " +
					            "dcterms:issued ?date . " +
					 "?hybridOrg org:hasUnit ?unit . " +
					 "?decision2 elod:seller ?hybridOrg ; " +
					 			"dcterms:issued ?date . " +
					 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
					 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
					 "FILTER NOT EXISTS {?decision1 elod:hasCorrectedDecision ?correctedDecision} . " +
					 "FILTER NOT EXISTS {?decision2 elod:hasCorrectedDecision ?correctedDecision} . " +
					 "FILTER EXISTS {?org rdf:type gr:BusinessEntity} . " +
					 "} " +
				 "UNION " +
					 "{ " +
					 "?decision1 dc:publisher ?unit ; " +
					            "dcterms:issued ?date . " +
					 "?unit rdfs:seeAlso ?hybridOrg . " +
					 "?decision2 elod:seller ?hybridOrg ; " +
			 					"dcterms:issued ?date . " +
					 "FILTER (?date >= \"" + QueryConfiguration.dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
					 "FILTER (?date < \"" + QueryConfiguration.dateFilterTo + "T00:00:00Z\"^^xsd:dateTime) . " +
					 "FILTER NOT EXISTS {?decision1 elod:hasCorrectedDecision ?correctedDecision} . " +
					 "FILTER NOT EXISTS {?decision2 elod:hasCorrectedDecision ?correctedDecision} . " +
					 "} " +
				 "}";
		
		VirtuosoQueryExecution vqeHybrids = VirtuosoQueryExecutionFactory.create(queryHybrids, graphDiavgeiaII);
		ResultSet resultsHybrids = vqeHybrids.execSelect();
		
		while (resultsHybrids.hasNext()) {
			QuerySolution result = resultsHybrids.nextSolution();
			if ( !((result.getResource("hybridOrg").getURI().contains("\"")) || 
					(result.getResource("hybridOrg").getURI().contains("`")) || 
					(result.getResource("hybridOrg").getURI().contains(" ")) || 
					(result.getResource("hybridOrg").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/"))|| 
					(result.getResource("hybridOrg").getURI().equalsIgnoreCase("http://linkedeconomy.org/resource/OrganizationalUnit/"))) ) {
				hybridsList.add(result.getResource("hybridOrg"));
			}
		}
		
		vqeHybrids.close();
		
		return hybridsList;
	}
	
	/** queries regarding both Buyers and Sellers **/
	/* Variation */
	public ArrayList<Resource> getOrgsWithStatistics(VirtGraph graphStats, String buyerOrSeller, String previousDate, String currentDate) {
		
		ArrayList<Resource> orgsList = new ArrayList<Resource>();
		
		String queryOrgs = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT DISTINCT ?org " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaIIStats + "> " +
					"WHERE { " +
					"?statistic elod:isStatisticOf ?org ; " +
								"elod:referenceTime ?referenceTime ; " +
								"elod:hasAggregate ?aggregateRes ; " +
								"elod:hasCounter ?counterRes ; " +
								"elod:hasRank ?rankRes ; " +
								"elod:orgActivity ?activity ; " +
								"elod:calculationDate ?calculationDate . " +
					"FILTER (?activity = \"" + buyerOrSeller + "\"^^xsd:string) . " +
					"FILTER (?referenceTime = \"" + previousDate + "\"^^xsd:date " +
							"|| ?referenceTime = \"" + currentDate + "\"^^xsd:date) . " +
					"}";

		VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory.create(queryOrgs, graphStats);
		ResultSet resultsOrgs = vqeOrgs.execSelect();
		
		while (resultsOrgs.hasNext()) {
			QuerySolution result = resultsOrgs.nextSolution();
			Resource org = result.getResource("org");
			orgsList.add(org);
		}
		vqeOrgs.close();
		
		return orgsList;
	}
	
	public String[] getOrgVariationCategoryDtls(VirtGraph graphStats, String orgUri, String dateFilter, String category, String buyerOrSeller) {
		
		Resource statistic = null;
		Resource aggregateAmountRes = null;
		Literal aggregatedAmount = null;
		Resource counterRes = null;
		Literal counter = null;
		Resource rankRes = null;
		Literal rank = null;
		String aggregatedAmountStr = null;
		boolean resultsFlag = false;
		
		String queryOrgCatDtls = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"SELECT ?statistic ?referenceTime ?aggregateRes (xsd:decimal(?amount) as ?aggregatedAmount) ?counterRes ?counter ?rankRes ?rank ?category ?activity " +
				"FROM <" + QueryConfiguration.queryGraphDiavgeiaIIStats + "> " +
				"WHERE { " +
				"?statistic elod:isStatisticOf <" + orgUri + "> ; " +
							"elod:referenceTime ?referenceTime ; " +
							"elod:hasAggregate ?aggregateRes ; " +
							"elod:hasCounter ?counterRes ; " +
							"elod:hasRank ?rankRes ; " +
							"elod:orgActivity ?activity . " +
				"?aggregateRes elod:hasCategory ?category ; " +
							  "elod:aggregatedAmount ?amount . " +
				"?counterRes elod:hasCategory ?category ; " +
							"elod:counter ?counter . " +
				"?rankRes elod:hasCategory ?category ; " +
						 "elod:rank ?rank . " +
				"FILTER (?referenceTime = \"" + dateFilter + "\"^^xsd:date) . " + //xsd:gYear
				"FILTER (?category = <http://linkedeconomy.org/ontology#" + category + ">) . " +
				"FILTER (?activity = \"" + buyerOrSeller + "\"^^xsd:string) . " +
				"}";

		VirtuosoQueryExecution vqeOrgCatDtls = VirtuosoQueryExecutionFactory.create(queryOrgCatDtls, graphStats);
		ResultSet resultsOrgCatDtls = vqeOrgCatDtls.execSelect();
		
		if (resultsOrgCatDtls.hasNext()) {
			QuerySolution result = resultsOrgCatDtls.nextSolution();
			statistic = result.getResource("statistic");
			aggregateAmountRes = result.getResource("aggregateRes");
			aggregatedAmount = result.getLiteral("aggregatedAmount");
			counterRes = result.getResource("counterRes");
			counter = result.getLiteral("counter");
			rankRes = result.getResource("rankRes");
			rank = result.getLiteral("rank");
			aggregatedAmountStr = hm.roundAmount(aggregatedAmount);
			resultsFlag = true;
		}
		
		vqeOrgCatDtls.close();
		
		if (resultsFlag) {
			return new String[]{statistic.toString(), aggregateAmountRes.toString(), aggregatedAmountStr, 
								counterRes.toString(), counter.getString(), rankRes.toString(), rank.getString()};
		} else {
			return new String[]{null, null, null, null, null, null, null};
		}
		
	}
	
	/** other queries **/
	public Literal getDataLastDate() {
		
		VirtGraph graphDiavgeiaII = new VirtGraph (QueryConfiguration.queryGraphDiavgeiaII, QueryConfiguration.connectionString, 
				  								   QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Graph!");
		
		Literal date = null;
		
		String queryDate = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT ?submissionTimestamp " +
					"FROM <" + QueryConfiguration.queryGraphDiavgeiaII + "> " +
					"WHERE { " +
					"?contract elod:submissionTimestamp ?submissionTimestamp . " +
					"} " +
					"ORDER BY DESC (?submissionTimestamp) " +
					"LIMIT 1";

		VirtuosoQueryExecution vqeDate = VirtuosoQueryExecutionFactory.create(queryDate, graphDiavgeiaII);
		ResultSet resultsDate = vqeDate.execSelect();
		
		if (resultsDate.hasNext()) {
			QuerySolution result = resultsDate.nextSolution();
			date = result.getLiteral("submissionTimestamp");
		}
		
		vqeDate.close();
		graphDiavgeiaII.close();
		
		return date;
	}
	
	private String checkForUnit(VirtGraph graphDiavgeiaII, String buyerUri) {
		
		String unitUri = null;
		
		String queryUnit = "SELECT ?unit " +
				"FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				"WHERE { " +
				"?unit rdfs:seeAlso <" + buyerUri + "> " +
				"}";

		VirtuosoQueryExecution vqeUnit = VirtuosoQueryExecutionFactory.create(queryUnit, graphDiavgeiaII);
		ResultSet resultsUnit = vqeUnit.execSelect();
		
		if (resultsUnit.hasNext()) {
			QuerySolution result = resultsUnit.nextSolution();
			unitUri = result.getResource("unit").getURI();
		}
		
		vqeUnit.close();
		
		return unitUri;
		
	}

}