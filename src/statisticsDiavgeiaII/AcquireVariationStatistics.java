package statisticsDiavgeiaII;

import com.hp.hpl.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.List;

import objects.StatisticVariationDtls;

import utils.HelperMethods;
import utils.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class AcquireVariationStatistics {

	public List<List<StatisticVariationDtls>> acquireBuyerFpmLists(String buyerOrSeller) {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
	
		/* Diavgeia II Statistics Graph */
		VirtGraph graphStats = new VirtGraph (QueryConfiguration.queryGraphDiavgeiaIIStats, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Statistics Graph!");
		
		List<List<StatisticVariationDtls>> buyerFpmList = new ArrayList<>();
		List<StatisticVariationDtls> committedList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> expApprsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> paymentList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> assignmentsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> noticesList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> awardsList = new ArrayList<StatisticVariationDtls>();
		
		/** Foreis **/
		System.out.println("Querying buyers (Foreis) statistics");
		ArrayList<Resource> buyersList = qs.getOrgsWithStatistics(graphStats, buyerOrSeller, QueryConfiguration.dateFilterPrevious, QueryConfiguration.dateFilterCurrent);
		
		for (Resource buyerUri : buyersList) {
			
			/** Committed: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Committed Dtls of: " + buyerUri.toString());
			String[] committedDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Committed", buyerOrSeller);
			String[] committedDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Committed", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(committedDtlsPrevious, committedDtlsCurrent) ) {
				float committedAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(committedDtlsPrevious[2]), Float.parseFloat(committedDtlsCurrent[2]));
				int committedCounterVariation = hm.calculateCounterVariation(Integer.parseInt(committedDtlsPrevious[4]), Integer.parseInt(committedDtlsCurrent[4]));
				int committedRankVariation = hm.calculateRankVariation(Integer.parseInt(committedDtlsPrevious[6]), Integer.parseInt(committedDtlsCurrent[6]));
				StatisticVariationDtls committedObj = new StatisticVariationDtls("Committed", committedDtlsCurrent[0], buyerUri, committedDtlsCurrent[1], committedAmountVariation, 
																				 committedDtlsCurrent[3], committedCounterVariation, committedDtlsCurrent[5], committedRankVariation);
				committedList.add(committedObj);
			}
			
			/** Expense Approval: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Expense Approval Dtls of: " + buyerUri.toString());
			String[] expApprsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "ExpenseApproval", buyerOrSeller);
			String[] expApprsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "ExpenseApproval", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(expApprsDtlsPrevious, expApprsDtlsCurrent) ) {
				float expApprsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(expApprsDtlsPrevious[2]), Float.parseFloat(expApprsDtlsCurrent[2]));
				int expApprsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(expApprsDtlsPrevious[4]), Integer.parseInt(expApprsDtlsCurrent[4]));
				int expApprsRankVariation = hm.calculateRankVariation(Integer.parseInt(expApprsDtlsPrevious[6]), Integer.parseInt(expApprsDtlsCurrent[6]));
				StatisticVariationDtls expApprsObj = new StatisticVariationDtls("ExpenseApproval", expApprsDtlsCurrent[0], buyerUri, expApprsDtlsCurrent[1], expApprsAmountVariation, 
																				expApprsDtlsCurrent[3], expApprsCounterVariation, expApprsDtlsCurrent[5], expApprsRankVariation);
				expApprsList.add(expApprsObj);
			}
			
			/** Payment: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Payment Dtls of: " + buyerUri.toString());
			String[] paymentDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Payment", buyerOrSeller);
			String[] paymentDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Payment", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(paymentDtlsPrevious, paymentDtlsCurrent) ) {
				float paymentAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(paymentDtlsPrevious[2]), Float.parseFloat(paymentDtlsCurrent[2]));
				int paymentCounterVariation = hm.calculateCounterVariation(Integer.parseInt(paymentDtlsPrevious[4]), Integer.parseInt(paymentDtlsCurrent[4]));
				int paymentRankVariation = hm.calculateRankVariation(Integer.parseInt(paymentDtlsPrevious[6]), Integer.parseInt(paymentDtlsCurrent[6]));
				StatisticVariationDtls paymentObj = new StatisticVariationDtls("Payment", paymentDtlsCurrent[0], buyerUri, paymentDtlsCurrent[1], paymentAmountVariation, 
																			   paymentDtlsCurrent[3], paymentCounterVariation, paymentDtlsCurrent[5], paymentRankVariation);
				paymentList.add(paymentObj);
			}
			
			/** Assignments: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Assignments Dtls of: " + buyerUri.toString());
			String[] assignmentsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Assignment", buyerOrSeller);
			String[] assignmentsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Assignment", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(assignmentsDtlsPrevious, assignmentsDtlsCurrent) ) {
				float assignmentsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(assignmentsDtlsPrevious[2]), Float.parseFloat(assignmentsDtlsCurrent[2]));
				int assignmentsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(assignmentsDtlsPrevious[4]), Integer.parseInt(assignmentsDtlsCurrent[4]));
				int assignmentsRankVariation = hm.calculateRankVariation(Integer.parseInt(assignmentsDtlsPrevious[6]), Integer.parseInt(assignmentsDtlsCurrent[6]));
				StatisticVariationDtls assignmentsObj = new StatisticVariationDtls("Assignment", assignmentsDtlsCurrent[0], buyerUri, assignmentsDtlsCurrent[1], assignmentsAmountVariation, 
																			   	   assignmentsDtlsCurrent[3], assignmentsCounterVariation, assignmentsDtlsCurrent[5], assignmentsRankVariation);
				assignmentsList.add(assignmentsObj);
			}
			
			/** Notices: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Notices Dtls of: " + buyerUri.toString());
			String[] noticesDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Notice", buyerOrSeller);
			String[] noticesDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Notice", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(noticesDtlsPrevious, noticesDtlsCurrent) ) {
				float noticesAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(noticesDtlsPrevious[2]), Float.parseFloat(noticesDtlsCurrent[2]));
				int noticesCounterVariation = hm.calculateCounterVariation(Integer.parseInt(noticesDtlsPrevious[4]), Integer.parseInt(noticesDtlsCurrent[4]));
				int noticesRankVariation = hm.calculateRankVariation(Integer.parseInt(noticesDtlsPrevious[6]), Integer.parseInt(noticesDtlsCurrent[6]));
				StatisticVariationDtls noticesObj = new StatisticVariationDtls("Notice", noticesDtlsCurrent[0], buyerUri, noticesDtlsCurrent[1], noticesAmountVariation, 
																			   noticesDtlsCurrent[3], noticesCounterVariation, noticesDtlsCurrent[5], noticesRankVariation);
				noticesList.add(noticesObj);
			}
			
			/** Awards: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Awards Dtls of: " + buyerUri.toString());
			String[] awardsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Award", buyerOrSeller);
			String[] awardsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Award", buyerOrSeller);
			if ( !hm.checkStringArraysForNull(awardsDtlsPrevious, awardsDtlsCurrent) ) {
				float awardsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(awardsDtlsPrevious[2]), Float.parseFloat(awardsDtlsCurrent[2]));
				int awardsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(awardsDtlsPrevious[4]), Integer.parseInt(awardsDtlsCurrent[4]));
				int awardsRankVariation = hm.calculateRankVariation(Integer.parseInt(awardsDtlsPrevious[6]), Integer.parseInt(awardsDtlsCurrent[6]));
				StatisticVariationDtls awardsObj = new StatisticVariationDtls("Award", awardsDtlsCurrent[0], buyerUri, awardsDtlsCurrent[1], awardsAmountVariation, 
																			  awardsDtlsCurrent[3], awardsCounterVariation, awardsDtlsCurrent[5], awardsRankVariation);
				awardsList.add(awardsObj);
			}
		}
		
		buyerFpmList.add(committedList);
		buyerFpmList.add(expApprsList);
		buyerFpmList.add(paymentList);
		buyerFpmList.add(assignmentsList);
		buyerFpmList.add(noticesList);
		buyerFpmList.add(awardsList);
		
		graphStats.close();
		
		return buyerFpmList;
	}
	
	public List<List<StatisticVariationDtls>> acquireSellerFpmLists(String buyerOrSeller) {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
	
		/* Diavgeia II Statistics Graph */
		VirtGraph graphStats = new VirtGraph (QueryConfiguration.queryGraphDiavgeiaIIStats, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Statistics Graph!");
		
		List<List<StatisticVariationDtls>> sellerFpmList = new ArrayList<>();
		List<StatisticVariationDtls> expApprsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> paymentList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> assignmentsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> awardsList = new ArrayList<StatisticVariationDtls>();
		
		/** Foreis **/
		System.out.println("Querying sellers (Anadoxoi) statistics");
		ArrayList<Resource> sellersList = qs.getOrgsWithStatistics(graphStats, buyerOrSeller, QueryConfiguration.dateFilterPrevious, QueryConfiguration.dateFilterCurrent);
		
		for (Resource sellerUri : sellersList) {
			
			if (!(sellerUri.toString().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/..") || 
				  sellerUri.toString().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/."))) {
				
				/** Expense Approval: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Expense Approval Dtls of: " + sellerUri.toString());
				String[] expApprsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "ExpenseApproval", buyerOrSeller);
				String[] expApprsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "ExpenseApproval", buyerOrSeller);
				if ( !hm.checkStringArraysForNull(expApprsDtlsPrevious, expApprsDtlsCurrent) ) {
					float expApprsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(expApprsDtlsPrevious[2]), Float.parseFloat(expApprsDtlsCurrent[2]));
					int expApprsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(expApprsDtlsPrevious[4]), Integer.parseInt(expApprsDtlsCurrent[4]));
					int expApprsRankVariation = hm.calculateRankVariation(Integer.parseInt(expApprsDtlsPrevious[6]), Integer.parseInt(expApprsDtlsCurrent[6]));
					StatisticVariationDtls expApprsObj = new StatisticVariationDtls("ExpenseApproval", expApprsDtlsCurrent[0], sellerUri, expApprsDtlsCurrent[1], expApprsAmountVariation, 
															 						expApprsDtlsCurrent[3], expApprsCounterVariation, expApprsDtlsCurrent[5], expApprsRankVariation);
					expApprsList.add(expApprsObj);
				}
				
				/** Payment: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Payment Dtls of: " + sellerUri.toString());
				String[] paymentDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Payment", buyerOrSeller);
				String[] paymentDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Payment", buyerOrSeller);
				if ( !hm.checkStringArraysForNull(paymentDtlsPrevious, paymentDtlsCurrent) ) {
					float paymentAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(paymentDtlsPrevious[2]), Float.parseFloat(paymentDtlsCurrent[2]));
					int paymentCounterVariation = hm.calculateCounterVariation(Integer.parseInt(paymentDtlsPrevious[4]), Integer.parseInt(paymentDtlsCurrent[4]));
					int paymentRankVariation = hm.calculateRankVariation(Integer.parseInt(paymentDtlsPrevious[6]), Integer.parseInt(paymentDtlsCurrent[6]));
					StatisticVariationDtls paymentObj = new StatisticVariationDtls("Payment", paymentDtlsCurrent[0], sellerUri, paymentDtlsCurrent[1], paymentAmountVariation, 
																				   paymentDtlsCurrent[3], paymentCounterVariation, paymentDtlsCurrent[5], paymentRankVariation);
					paymentList.add(paymentObj);
				}
				
				/** Assignments: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Assignments Dtls of: " + sellerUri.toString());
				String[] assignmentsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Assignment", buyerOrSeller);
				String[] assignmentsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Assignment", buyerOrSeller);
				if ( !hm.checkStringArraysForNull(assignmentsDtlsPrevious, assignmentsDtlsCurrent) ) {
					float assignmentsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(assignmentsDtlsPrevious[2]), Float.parseFloat(assignmentsDtlsCurrent[2]));
					int assignmentsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(assignmentsDtlsPrevious[4]), Integer.parseInt(assignmentsDtlsCurrent[4]));
					int assignmentsRankVariation = hm.calculateRankVariation(Integer.parseInt(assignmentsDtlsPrevious[6]), Integer.parseInt(assignmentsDtlsCurrent[6]));
					StatisticVariationDtls assignmentsObj = new StatisticVariationDtls("Assignment", assignmentsDtlsCurrent[0], sellerUri, assignmentsDtlsCurrent[1], assignmentsAmountVariation, 
																				   	   assignmentsDtlsCurrent[3], assignmentsCounterVariation, assignmentsDtlsCurrent[5], assignmentsRankVariation);
					assignmentsList.add(assignmentsObj);
				}
				
				/** Awards: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Awards Dtls of: " + sellerUri.toString());
				String[] awardsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Award", buyerOrSeller);
				String[] awardsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Award", buyerOrSeller);
				if ( !hm.checkStringArraysForNull(awardsDtlsPrevious, awardsDtlsCurrent) ) {
					float awardsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(awardsDtlsPrevious[2]), Float.parseFloat(awardsDtlsCurrent[2]));
					int awardsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(awardsDtlsPrevious[4]), Integer.parseInt(awardsDtlsCurrent[4]));
					int awardsRankVariation = hm.calculateRankVariation(Integer.parseInt(awardsDtlsPrevious[6]), Integer.parseInt(awardsDtlsCurrent[6]));
					StatisticVariationDtls awardsObj = new StatisticVariationDtls("Award", awardsDtlsCurrent[0], sellerUri, awardsDtlsCurrent[1], awardsAmountVariation, 
																				  awardsDtlsCurrent[3], awardsCounterVariation, awardsDtlsCurrent[5], awardsRankVariation);
					awardsList.add(awardsObj);
				}
			}
		}
		
		sellerFpmList.add(expApprsList);
		sellerFpmList.add(paymentList);
		sellerFpmList.add(assignmentsList);
		sellerFpmList.add(awardsList);
		
		return sellerFpmList;
	}
	
	public List<List<StatisticVariationDtls>> acquireHybridBuyerFpmLists() {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
	
		/* Diavgeia II Statistics Graph */
		VirtGraph graphStats = new VirtGraph (QueryConfiguration.queryGraphDiavgeiaIIStats, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Statistics Graph!");
		
		List<List<StatisticVariationDtls>> buyerFpmList = new ArrayList<>();
		List<StatisticVariationDtls> committedList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> expApprsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> paymentList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> assignmentsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> noticesList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> awardsList = new ArrayList<StatisticVariationDtls>();
		
		/** Foreis **/
		System.out.println("Querying Hybrids (as buyer) statistics");
		ArrayList<Resource> buyersList = qs.getOrgsWithStatistics(graphStats, "HybridBuyer", QueryConfiguration.dateFilterPrevious, QueryConfiguration.dateFilterCurrent);
		
		for (Resource buyerUri : buyersList) {
			
			/** Committed: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Contract Dtls of: " + buyerUri.toString());
			String[] committedDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Committed", "Buyer");
			String[] committedDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Committed", "Buyer");
			if ( !hm.checkStringArraysForNull(committedDtlsPrevious, committedDtlsCurrent) ) {
				float committedAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(committedDtlsPrevious[2]), Float.parseFloat(committedDtlsCurrent[2]));
				int committedCounterVariation = hm.calculateCounterVariation(Integer.parseInt(committedDtlsPrevious[4]), Integer.parseInt(committedDtlsCurrent[4]));
				int committedRankVariation = hm.calculateRankVariation(Integer.parseInt(committedDtlsPrevious[6]), Integer.parseInt(committedDtlsCurrent[6]));
				StatisticVariationDtls committedObj = new StatisticVariationDtls("Committed", committedDtlsCurrent[0], buyerUri, committedDtlsCurrent[1], committedAmountVariation, 
																				 committedDtlsCurrent[3], committedCounterVariation, committedDtlsCurrent[5], committedRankVariation);
				committedList.add(committedObj);
			}
			
			/** Expense Approval: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Contract Dtls of: " + buyerUri.toString());
			String[] expApprsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "ExpenseApproval", "Buyer");
			String[] expApprsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "ExpenseApproval", "Buyer");
			if ( !hm.checkStringArraysForNull(expApprsDtlsPrevious, expApprsDtlsCurrent) ) {
				float expApprsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(expApprsDtlsPrevious[2]), Float.parseFloat(expApprsDtlsCurrent[2]));
				int expApprsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(expApprsDtlsPrevious[4]), Integer.parseInt(expApprsDtlsCurrent[4]));
				int expApprsRankVariation = hm.calculateRankVariation(Integer.parseInt(expApprsDtlsPrevious[6]), Integer.parseInt(expApprsDtlsCurrent[6]));
				StatisticVariationDtls expApprsObj = new StatisticVariationDtls("ExpenseApproval", expApprsDtlsCurrent[0], buyerUri, expApprsDtlsCurrent[1], expApprsAmountVariation, 
																				expApprsDtlsCurrent[3], expApprsCounterVariation, expApprsDtlsCurrent[5], expApprsRankVariation);
				expApprsList.add(expApprsObj);
			}
			
			/** Payment: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Payment Dtls of: " + buyerUri.toString());
			String[] paymentDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Payment", "Buyer");
			String[] paymentDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Payment", "Buyer");
			if ( !hm.checkStringArraysForNull(paymentDtlsPrevious, paymentDtlsCurrent) ) {
				float paymentAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(paymentDtlsPrevious[2]), Float.parseFloat(paymentDtlsCurrent[2]));
				int paymentCounterVariation = hm.calculateCounterVariation(Integer.parseInt(paymentDtlsPrevious[4]), Integer.parseInt(paymentDtlsCurrent[4]));
				int paymentRankVariation = hm.calculateRankVariation(Integer.parseInt(paymentDtlsPrevious[6]), Integer.parseInt(paymentDtlsCurrent[6]));
				StatisticVariationDtls paymentObj = new StatisticVariationDtls("Payment", paymentDtlsCurrent[0], buyerUri, paymentDtlsCurrent[1], paymentAmountVariation, 
																			   paymentDtlsCurrent[3], paymentCounterVariation, paymentDtlsCurrent[5], paymentRankVariation);
				paymentList.add(paymentObj);
			}
			
			/** Assignments: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Payment Dtls of: " + buyerUri.toString());
			String[] assignmentsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Assignment", "Buyer");
			String[] assignmentsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Assignment", "Buyer");
			if ( !hm.checkStringArraysForNull(assignmentsDtlsPrevious, assignmentsDtlsCurrent) ) {
				float assignmentsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(assignmentsDtlsPrevious[2]), Float.parseFloat(assignmentsDtlsCurrent[2]));
				int assignmentsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(assignmentsDtlsPrevious[4]), Integer.parseInt(assignmentsDtlsCurrent[4]));
				int assignmentsRankVariation = hm.calculateRankVariation(Integer.parseInt(assignmentsDtlsPrevious[6]), Integer.parseInt(assignmentsDtlsCurrent[6]));
				StatisticVariationDtls assignmentsObj = new StatisticVariationDtls("Assignment", assignmentsDtlsCurrent[0], buyerUri, assignmentsDtlsCurrent[1], assignmentsAmountVariation, 
																			   	   assignmentsDtlsCurrent[3], assignmentsCounterVariation, assignmentsDtlsCurrent[5], assignmentsRankVariation);
				assignmentsList.add(assignmentsObj);
			}
			
			/** Notices: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Payment Dtls of: " + buyerUri.toString());
			String[] noticesDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Notice", "Buyer");
			String[] noticesDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Notice", "Buyer");
			if ( !hm.checkStringArraysForNull(noticesDtlsPrevious, noticesDtlsCurrent) ) {
				float noticesAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(noticesDtlsPrevious[2]), Float.parseFloat(noticesDtlsCurrent[2]));
				int noticesCounterVariation = hm.calculateCounterVariation(Integer.parseInt(noticesDtlsPrevious[4]), Integer.parseInt(noticesDtlsCurrent[4]));
				int noticesRankVariation = hm.calculateRankVariation(Integer.parseInt(noticesDtlsPrevious[6]), Integer.parseInt(noticesDtlsCurrent[6]));
				StatisticVariationDtls noticesObj = new StatisticVariationDtls("Notice", noticesDtlsCurrent[0], buyerUri, noticesDtlsCurrent[1], noticesAmountVariation, 
																			   noticesDtlsCurrent[3], noticesCounterVariation, noticesDtlsCurrent[5], noticesRankVariation);
				noticesList.add(noticesObj);
			}
			
			/** Awards: Amount, Counter, Rank, Variation **/
			System.out.println("Querying Payment Dtls of: " + buyerUri.toString());
			String[] awardsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterPrevious, "Award", "Buyer");
			String[] awardsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, buyerUri.toString(), QueryConfiguration.dateFilterCurrent, "Award", "Buyer");
			if ( !hm.checkStringArraysForNull(awardsDtlsPrevious, awardsDtlsCurrent) ) {
				float awardsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(awardsDtlsPrevious[2]), Float.parseFloat(awardsDtlsCurrent[2]));
				int awardsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(awardsDtlsPrevious[4]), Integer.parseInt(awardsDtlsCurrent[4]));
				int awardsRankVariation = hm.calculateRankVariation(Integer.parseInt(awardsDtlsPrevious[6]), Integer.parseInt(awardsDtlsCurrent[6]));
				StatisticVariationDtls awardsObj = new StatisticVariationDtls("Award", awardsDtlsCurrent[0], buyerUri, awardsDtlsCurrent[1], awardsAmountVariation, 
																			  awardsDtlsCurrent[3], awardsCounterVariation, awardsDtlsCurrent[5], awardsRankVariation);
				awardsList.add(awardsObj);
			}
		}
		
		buyerFpmList.add(committedList);
		buyerFpmList.add(expApprsList);
		buyerFpmList.add(paymentList);
		buyerFpmList.add(assignmentsList);
		buyerFpmList.add(noticesList);
		buyerFpmList.add(awardsList);
		
		return buyerFpmList;
	}
	
	public List<List<StatisticVariationDtls>> acquireHybridSellerFpmLists() {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
	
		/* Diavgeia II Statistics Graph */
		VirtGraph graphStats = new VirtGraph (QueryConfiguration.queryGraphDiavgeiaIIStats, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Statistics Graph!");
		
		List<List<StatisticVariationDtls>> sellerFpmList = new ArrayList<>();
		List<StatisticVariationDtls> expApprsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> paymentList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> assignmentsList = new ArrayList<StatisticVariationDtls>();
		List<StatisticVariationDtls> awardsList = new ArrayList<StatisticVariationDtls>();
		
		/** Foreis **/
		System.out.println("Querying Hybrids (as sellers) statistics");
		ArrayList<Resource> sellersList = qs.getOrgsWithStatistics(graphStats, "HybridSeller", QueryConfiguration.dateFilterPrevious, QueryConfiguration.dateFilterCurrent);
		
		for (Resource sellerUri : sellersList) {
			
			if (!(sellerUri.toString().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/..") || 
				  sellerUri.toString().equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/."))) {
				
				/** Expense Approval: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Contract Dtls of: " + sellerUri.toString());
				String[] expApprsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "ExpenseApproval", "Seller");
				String[] expApprsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "ExpenseApproval", "Seller");
				if ( !hm.checkStringArraysForNull(expApprsDtlsPrevious, expApprsDtlsCurrent) ) {
					float expApprsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(expApprsDtlsPrevious[2]), Float.parseFloat(expApprsDtlsCurrent[2]));
					int expApprsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(expApprsDtlsPrevious[4]), Integer.parseInt(expApprsDtlsCurrent[4]));
					int expApprsRankVariation = hm.calculateRankVariation(Integer.parseInt(expApprsDtlsPrevious[6]), Integer.parseInt(expApprsDtlsCurrent[6]));
					StatisticVariationDtls expApprsObj = new StatisticVariationDtls("ExpenseApproval", expApprsDtlsCurrent[0], sellerUri, expApprsDtlsCurrent[1], expApprsAmountVariation, 
															 						expApprsDtlsCurrent[3], expApprsCounterVariation, expApprsDtlsCurrent[5], expApprsRankVariation);
					expApprsList.add(expApprsObj);
				}
				
				/** Payment: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Payment Dtls of: " + sellerUri.toString());
				String[] paymentDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Payment", "Seller");
				String[] paymentDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Payment", "Seller");
				if ( !hm.checkStringArraysForNull(paymentDtlsPrevious, paymentDtlsCurrent) ) {
					float paymentAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(paymentDtlsPrevious[2]), Float.parseFloat(paymentDtlsCurrent[2]));
					int paymentCounterVariation = hm.calculateCounterVariation(Integer.parseInt(paymentDtlsPrevious[4]), Integer.parseInt(paymentDtlsCurrent[4]));
					int paymentRankVariation = hm.calculateRankVariation(Integer.parseInt(paymentDtlsPrevious[6]), Integer.parseInt(paymentDtlsCurrent[6]));
					StatisticVariationDtls paymentObj = new StatisticVariationDtls("Payment", paymentDtlsCurrent[0], sellerUri, paymentDtlsCurrent[1], paymentAmountVariation, 
																				   paymentDtlsCurrent[3], paymentCounterVariation, paymentDtlsCurrent[5], paymentRankVariation);
					paymentList.add(paymentObj);
				}
				
				/** Assignments: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Payment Dtls of: " + sellerUri.toString());
				String[] assignmentsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Assignment", "Seller");
				String[] assignmentsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Assignment", "Seller");
				if ( !hm.checkStringArraysForNull(assignmentsDtlsPrevious, assignmentsDtlsCurrent) ) {
					float assignmentsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(assignmentsDtlsPrevious[2]), Float.parseFloat(assignmentsDtlsCurrent[2]));
					int assignmentsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(assignmentsDtlsPrevious[4]), Integer.parseInt(assignmentsDtlsCurrent[4]));
					int assignmentsRankVariation = hm.calculateRankVariation(Integer.parseInt(assignmentsDtlsPrevious[6]), Integer.parseInt(assignmentsDtlsCurrent[6]));
					StatisticVariationDtls assignmentsObj = new StatisticVariationDtls("Assignment", assignmentsDtlsCurrent[0], sellerUri, assignmentsDtlsCurrent[1], assignmentsAmountVariation, 
																				   	   assignmentsDtlsCurrent[3], assignmentsCounterVariation, assignmentsDtlsCurrent[5], assignmentsRankVariation);
					assignmentsList.add(assignmentsObj);
				}
				
				/** Awards: Amount, Counter, Rank, Variation **/
				System.out.println("Querying Payment Dtls of: " + sellerUri.toString());
				String[] awardsDtlsPrevious = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterPrevious, "Award", "Seller");
				String[] awardsDtlsCurrent = qs.getOrgVariationCategoryDtls(graphStats, sellerUri.toString(), QueryConfiguration.dateFilterCurrent, "Award", "Seller");
				if ( !hm.checkStringArraysForNull(awardsDtlsPrevious, awardsDtlsCurrent) ) {
					float awardsAmountVariation = hm.calculatePercentageVariation(Float.parseFloat(awardsDtlsPrevious[2]), Float.parseFloat(awardsDtlsCurrent[2]));
					int awardsCounterVariation = hm.calculateCounterVariation(Integer.parseInt(awardsDtlsPrevious[4]), Integer.parseInt(awardsDtlsCurrent[4]));
					int awardsRankVariation = hm.calculateRankVariation(Integer.parseInt(awardsDtlsPrevious[6]), Integer.parseInt(awardsDtlsCurrent[6]));
					StatisticVariationDtls awardsObj = new StatisticVariationDtls("Award", awardsDtlsCurrent[0], sellerUri, awardsDtlsCurrent[1], awardsAmountVariation, 
																				  awardsDtlsCurrent[3], awardsCounterVariation, awardsDtlsCurrent[5], awardsRankVariation);
					awardsList.add(awardsObj);
				}
			}
		}
		
		sellerFpmList.add(expApprsList);
		sellerFpmList.add(paymentList);
		sellerFpmList.add(assignmentsList);
		sellerFpmList.add(awardsList);
		
		graphStats.close();
		
		return sellerFpmList;
	}
	
}