package statisticsDiavgeiaII;

import com.hp.hpl.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import objects.StatisticAcrDtls;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class AcquireAcrStatistics {

	public List<List<StatisticAcrDtls>> acquireBuyerFpmLists(VirtGraph graphDiavgeiaII, ArrayList<Resource> buyersList) {
		
		Queries qs = new Queries();
	
		List<List<StatisticAcrDtls>> buyerFpmList = new ArrayList<>();
		List<StatisticAcrDtls> committedList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> expenseApprovalList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> paymentsList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> assignmentsList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> noticesList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> awardsList = new ArrayList<StatisticAcrDtls>();
		
		for (Resource buyerUri : buyersList) {

			/** Committed: Amount, Counter **/
			System.out.println("Querying Committed of: " + buyerUri.toString());
			String[] committedDtls = qs.getBuyerCommittedDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls committedObj = new StatisticAcrDtls("Committed", buyerUri, committedDtls[0], Float.parseFloat(committedDtls[1]));
			committedList.add(committedObj);

			/** Expense Approval: Amount, Counter **/
			System.out.println("Querying Expense Approvals of: " + buyerUri.toString());
			String[] expApprDtls = qs.getBuyerExpenseApprovalDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls expApprObj = new StatisticAcrDtls("ExpenseApproval", buyerUri, expApprDtls[0], Float.parseFloat(expApprDtls[1]));
			expenseApprovalList.add(expApprObj);

			/** Payments: Amount, Counter **/
			System.out.println("Querying Payments of: " + buyerUri.toString());
			String[] paymentsDtls = qs.getBuyerPaymentDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls paymentObj = new StatisticAcrDtls("Payment", buyerUri, paymentsDtls[0], Float.parseFloat(paymentsDtls[1]));
			paymentsList.add(paymentObj);

			/** Assignments: Amount, Counter **/
			System.out.println("Querying Assignments of: " + buyerUri.toString());
			String[] assignmentsDtls = qs.getBuyerAssignmentDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls assignmentObj = new StatisticAcrDtls("Assignment", buyerUri, assignmentsDtls[0], Float.parseFloat(assignmentsDtls[1]));
			assignmentsList.add(assignmentObj);

			/** Notices: Amount, Counter **/
			System.out.println("Querying Notices of: " + buyerUri.toString());
			String[] noticesDtls = qs.getBuyerNoticeDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls noticeObj = new StatisticAcrDtls("Notice", buyerUri, noticesDtls[0], Float.parseFloat(noticesDtls[1]));
			noticesList.add(noticeObj);

			/** Awards: Amount, Counter **/
			System.out.println("Querying Awards of: " + buyerUri.toString());
			String[] awardsDtls = qs.getBuyerAwardDtls(graphDiavgeiaII, buyerUri.toString());
			StatisticAcrDtls awardObj = new StatisticAcrDtls("Award", buyerUri, awardsDtls[0], Float.parseFloat(awardsDtls[1]));
			awardsList.add(awardObj);

		}
		
		/* order by descending amount and add rank */
		/** Committed B.1.3 **/
		Collections.sort(committedList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < committedList.size(); i++) {
			System.out.println("Sorted Committed: " + committedList.get(i).getAggregatedAmount());
			if (committedList.get(i).getAggregatedAmount() > 0) {
				committedList.get(i).setRank(i + 1);
			}
		}
		
		/** Expense Approval B.2.1 **/
		Collections.sort(expenseApprovalList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < expenseApprovalList.size(); i++) {
			System.out.println("Sorted Expense Approvals: " + expenseApprovalList.get(i).getAggregatedAmount());
			if (expenseApprovalList.get(i).getAggregatedAmount() > 0) {
				expenseApprovalList.get(i).setRank(i + 1);
			}
		}
		
		/** Payments B.2.2 **/
		Collections.sort(paymentsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < paymentsList.size(); i++) {
			System.out.println("Sorted Payments: " + paymentsList.get(i).getAggregatedAmount());
			if (paymentsList.get(i).getAggregatedAmount() > 0) {
				paymentsList.get(i).setRank(i + 1);
			}
		}
		
		/** Assignments Δ.1 **/
		Collections.sort(assignmentsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < assignmentsList.size(); i++) {
			System.out.println("Sorted Assignments: " + assignmentsList.get(i).getAggregatedAmount());
			if (assignmentsList.get(i).getAggregatedAmount() > 0) {
				assignmentsList.get(i).setRank(i + 1);
			}
		}
		
		/** Notices Δ.2.1 **/
		Collections.sort(noticesList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < noticesList.size(); i++) {
			System.out.println("Sorted Notices: " + noticesList.get(i).getAggregatedAmount());
			if (noticesList.get(i).getAggregatedAmount() > 0) {
				noticesList.get(i).setRank(i + 1);
			}
		}
		
		/** Awards Δ.2.2 **/
		Collections.sort(awardsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < awardsList.size(); i++) {
			System.out.println("Sorted Awards: " + awardsList.get(i).getAggregatedAmount());
			if (awardsList.get(i).getAggregatedAmount() > 0) {
				awardsList.get(i).setRank(i + 1);
			}
		}
		
		buyerFpmList.add(committedList);
		buyerFpmList.add(expenseApprovalList);
		buyerFpmList.add(paymentsList);
		buyerFpmList.add(assignmentsList);
		buyerFpmList.add(noticesList);
		buyerFpmList.add(awardsList);
		
		return buyerFpmList;
	}
	
	public List<List<StatisticAcrDtls>> acquireSellerFpmLists(VirtGraph graphDiavgeiaII, ArrayList<Resource> sellersList) {
		
		Queries qs = new Queries();
		
		List<List<StatisticAcrDtls>> sellerFpmList = new ArrayList<>();
		List<StatisticAcrDtls> expenseApprovalList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> paymentsList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> assignmentsList = new ArrayList<StatisticAcrDtls>();
		List<StatisticAcrDtls> awardsList = new ArrayList<StatisticAcrDtls>();
		
		for (Resource sellerUri : sellersList) {

			/** Expense Approval: Amount, Counter **/
			System.out.println("Querying Expense Approval of: " + sellerUri.toString());
			String[] expApprsDtls = qs.getSellerExpenseApprovalsDtls(graphDiavgeiaII, sellerUri.toString());
			StatisticAcrDtls expApprObj = new StatisticAcrDtls("ExpenseApproval", sellerUri, expApprsDtls[0], Float.parseFloat(expApprsDtls[1]));
			expenseApprovalList.add(expApprObj);

			/** Payments: Amount, Counter **/
			System.out.println("Querying Payments of: " + sellerUri.toString());
			String[] paymentsDtls = qs.getSellerPaymentDtls(graphDiavgeiaII, sellerUri.toString());
			StatisticAcrDtls paymentObj = new StatisticAcrDtls("Payment", sellerUri, paymentsDtls[0], Float.parseFloat(paymentsDtls[1]));
			paymentsList.add(paymentObj);

			/** Assignments: Amount, Counter **/
			System.out.println("Querying Assignments of: " + sellerUri.toString());
			String[] assignmentsDtls = qs.getSellerAssignmentDtls(graphDiavgeiaII, sellerUri.toString());
			StatisticAcrDtls assignmentObj = new StatisticAcrDtls("Assignment", sellerUri, assignmentsDtls[0], Float.parseFloat(assignmentsDtls[1]));
			assignmentsList.add(assignmentObj);

			/** Awards: Amount, Counter **/
			System.out.println("Querying Awards of: " + sellerUri.toString());
			String[] awardsDtls = qs.getSellerAwardDtls(graphDiavgeiaII, sellerUri.toString());
			StatisticAcrDtls awardObj = new StatisticAcrDtls("Award", sellerUri, awardsDtls[0], Float.parseFloat(awardsDtls[1]));
			awardsList.add(awardObj);

		}
		
		/* order by descending amount and add rank */
		/** Expense Approval B.2.1 **/
		Collections.sort(expenseApprovalList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < expenseApprovalList.size(); i++) {
			System.out.println("Sorted Expense Approvals: " + expenseApprovalList.get(i).getAggregatedAmount());
			if (expenseApprovalList.get(i).getAggregatedAmount() > 0) {
				expenseApprovalList.get(i).setRank(i + 1);
			}
		}
		
		/** Payments B.2.2 **/
		Collections.sort(paymentsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < paymentsList.size(); i++) {
			System.out.println("Sorted Payments: " + paymentsList.get(i).getAggregatedAmount());
			if (paymentsList.get(i).getAggregatedAmount() > 0) {
				paymentsList.get(i).setRank(i + 1);
			}
		}
		
		/** Assignments Δ.1 **/
		Collections.sort(assignmentsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < assignmentsList.size(); i++) {
			System.out.println("Sorted Assignments: " + assignmentsList.get(i).getAggregatedAmount());
			if (assignmentsList.get(i).getAggregatedAmount() > 0) {
				assignmentsList.get(i).setRank(i + 1);
			}
		}
		
		/** Awards Δ.2.2 **/
		Collections.sort(awardsList, StatisticAcrDtls.getComparator());

		for (int i = 0; i < awardsList.size(); i++) {
			System.out.println("Sorted Awards: " + awardsList.get(i).getAggregatedAmount());
			if (awardsList.get(i).getAggregatedAmount() > 0) {
				awardsList.get(i).setRank(i + 1);
			}
		}
		
		sellerFpmList.add(expenseApprovalList);
		sellerFpmList.add(paymentsList);
		sellerFpmList.add(assignmentsList);
		sellerFpmList.add(awardsList);
		
		return sellerFpmList;
	}
	
}