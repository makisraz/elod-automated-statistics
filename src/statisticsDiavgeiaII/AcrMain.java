package statisticsDiavgeiaII;

import java.util.ArrayList;
import java.util.List;

import objects.StatisticAcrDtls;

import ontology.OntologyInitialization;

import utils.HelperMethods;
import utils.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class AcrMain {

	/** Steps for creating the Statistics for Aggregates, Counters and Rank
	 * 1) Set the referenceTime (QueryConfiguration) dateFilterFrom/dateFilterTo (e.g. 2013) 
	 *    and change at the of RdfActions class the value of referenceTime at line 51 and 
	 *    XSDDatatype at line 56 and duration at line 57
	 * 2) Query the Aggregated Amounts and the Counters of the 6 categories 
	 *    (Committed, ExpenseApproval, Payments, Assignment, Notice, Award)
	 * 3) Sort them and get the Ranks
	 * 4) Store the data of 2, 3 in the Graph
	 * 5) Repeat 2, 3, 4 for a new referenceTime (e.g. 2014) (Optional)
	 * 6) Upload the graph
	 * 7) Calculate the Variations using the VariationMain class
	 */
	public static void main(String[] args) {
		
		RdfActions rdfActions = new RdfActions();
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
		OntologyInitialization ontInit = new OntologyInitialization();
		AcquireAcrStatistics statistics = new AcquireAcrStatistics();
		
		Model model = rdfActions.remoteOrLocalModel(false);
		
		//Perform the basic initialization on the model
		ontInit.setPrefixes(model);
		ontInit.createHierarchies(model);
		ontInit.addIndividualsToModel(model);
		
		Literal lastUpdateDate = qs.getDataLastDate();
		String currentDate = hm.getCurrentDate();
		
		/* Diavgeia II Graph */
		VirtGraph graphDiavgeiaII = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII, QueryConfiguration.connectionString, 
											  	  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Diavgeia II Graph!");
		
		/** Buyers **/
		if (QueryConfiguration.buyers) {
			
			//find the Buyers (Foreis)
			System.out.println("Querying Buyers (Foreis)");
			ArrayList<Resource> buyersList = qs.getAllBuyers(graphDiavgeiaII);
			
			//acquire buyer statistics
			List<List<StatisticAcrDtls>> buyerFpmList = statistics.acquireBuyerFpmLists(graphDiavgeiaII, buyersList);
			
			//iterate the Buyers list of lists of objects
			for (List<StatisticAcrDtls> statisticsAcrList : buyerFpmList) {
				
				for (StatisticAcrDtls stats : statisticsAcrList) {
					rdfActions.createAcrStatistics(stats, model, lastUpdateDate, "Buyer", currentDate);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
		}
		
		/** Sellers **/
		if (QueryConfiguration.sellers) {
			
			//find the Sellers (Anadoxoi)
			System.out.println("Querying Sellers (Anadoxoi)");
			ArrayList<Resource> sellersList = qs.getAllSellers(graphDiavgeiaII);
			
			//acquire seller statistics
			List<List<StatisticAcrDtls>> sellerFpmList = statistics.acquireSellerFpmLists(graphDiavgeiaII, sellersList);
			
			//iterate the Sellers list of lists of objects
			for (List<StatisticAcrDtls> statisticsAcrList : sellerFpmList) {
		
				for (StatisticAcrDtls stats : statisticsAcrList) {
					rdfActions.createAcrStatistics(stats, model, lastUpdateDate, "Seller", currentDate);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
		}
		
		/** Hybrids **/
		if (QueryConfiguration.hybrids) {
			
			//find the Hybrid Organizations
			System.out.println("Querying Hybrid Organizations");
			ArrayList<Resource> hybridsList = qs.getAllHybrids(graphDiavgeiaII);
			
			/** First as Buyers **/
			//acquire hybrid statistics
			List<List<StatisticAcrDtls>> hybridFpmList = statistics.acquireBuyerFpmLists(graphDiavgeiaII, hybridsList);
			
			//iterate the Hybrids list of lists of objects
			for (List<StatisticAcrDtls> statisticsAcrList : hybridFpmList) {
		
				for (StatisticAcrDtls stats : statisticsAcrList) {
					rdfActions.createAcrStatistics(stats, model, lastUpdateDate, "HybridBuyer", currentDate);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
			
			/** Then as Sellers **/
			//acquire hybrid statistics
			hybridFpmList = statistics.acquireSellerFpmLists(graphDiavgeiaII, hybridsList);
			
			//iterate the Hybrids list of lists of objects
			for (List<StatisticAcrDtls> statisticsAcrList : hybridFpmList) {
		
				for (StatisticAcrDtls stats : statisticsAcrList) {
					rdfActions.createAcrStatistics(stats, model, lastUpdateDate, "HybridSeller", currentDate);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
			
		}
		
		//close the model
		model.close();
		
		graphDiavgeiaII.close();
	}
	
}