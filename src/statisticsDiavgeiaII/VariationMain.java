package statisticsDiavgeiaII;

import java.util.List;

import objects.StatisticVariationDtls;

import ontology.OntologyInitialization;

import utils.QueryConfiguration;

import com.hp.hpl.jena.rdf.model.Model;

/**
 * @author G. Razis
 */
public class VariationMain {

	/** Steps for creating the Statistics of Variations of Aggregates, Counters and Rank
	 * 1) Assuming the AcrMain is completed and graph is uploaded
	 * 2) Set (QueryConfiguration) dateFilterPrevious/dateFilterCurrent of required Statistics
	 * 3) Depending on date filters type adjust the filters of the (Queries) getOrgVariationCategoryDtls 
	 *    method (xsd:date OR xsd:gYear)
	 * 4) Calculate the Variations by running this class
	 * 5) Upload the generated graph
	 */
	public static void main(String[] args) {
		
		RdfActions rdfActions = new RdfActions();
		OntologyInitialization ontInit = new OntologyInitialization();
		AcquireVariationStatistics statistics = new AcquireVariationStatistics();
		
		Model model = rdfActions.remoteOrLocalModel(false);
		
		//Perform the basic initialization on the model
		ontInit.setPrefixes(model);
		ontInit.createHierarchies(model);
		ontInit.addIndividualsToModel(model);
		
		/** Buyers **/
		if (QueryConfiguration.buyers) {
			//acquire buyers with statistics
			List<List<StatisticVariationDtls>> buyerFpmList = statistics.acquireBuyerFpmLists("Buyer");
			
			//iterate the Buyers list of lists of objects
			for (List<StatisticVariationDtls> statisticsList : buyerFpmList) {
				
				for (StatisticVariationDtls stats : statisticsList) {
					rdfActions.createVariationStatistics(stats, model);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
		}
		
		/** Sellers **/
		if (QueryConfiguration.sellers) {
			//acquire sellers with statistics
			List<List<StatisticVariationDtls>> sellerFpmList = statistics.acquireSellerFpmLists("Seller");
			
			//iterate the Buyers list of lists of objects
			for (List<StatisticVariationDtls> statisticsList : sellerFpmList) {
						
				for (StatisticVariationDtls stats : statisticsList) {
					rdfActions.createVariationStatistics(stats, model);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
		}
		
		/** Hybrids **/
		if (QueryConfiguration.hybrids) {
			
			/** First as Buyers **/
			//acquire sellers with statistics
			List<List<StatisticVariationDtls>> hybridFpmList = statistics.acquireBuyerFpmLists("HybridBuyer");
			
			//iterate the Buyers list of lists of objects
			for (List<StatisticVariationDtls> statisticsList : hybridFpmList) {
						
				for (StatisticVariationDtls stats : statisticsList) {
					rdfActions.createVariationStatistics(stats, model);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
			
			/** Then as Sellers **/
			//acquire sellers with statistics
			hybridFpmList = statistics.acquireSellerFpmLists("HybridSeller");
			
			//iterate the Buyers list of lists of objects
			for (List<StatisticVariationDtls> statisticsList : hybridFpmList) {
						
				for (StatisticVariationDtls stats : statisticsList) {
					rdfActions.createVariationStatistics(stats, model);
				}
				
				//save the model after each list
				rdfActions.writeModel(model);
			}
		}
		
		//close the model
		model.close();
	}
	
}