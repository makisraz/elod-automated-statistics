package objects;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class StatisticVariationDtls {

	private String category = null;
	private String statisticUri = null;
	private Resource organizationUri = null;
	private String aggregateAmountUri = null;
	private float variationAggregatedAmount= -1;
	private String counterUri = null;
	private int variationCounter = -1;
	private String rankUri = null;
	private int variationRank = -1;

	public StatisticVariationDtls(String category, String statisticUri, Resource organizationUri, String aggregateAmountUri, float variationAggregatedAmount, 
			 					  String counterUri, int variationCounter, String rankUri, int variationRank) {
		
		this.category = category;
		this.statisticUri = statisticUri;
		this.organizationUri = organizationUri;
		this.aggregateAmountUri = aggregateAmountUri;
		this.variationAggregatedAmount = variationAggregatedAmount;
		this.counterUri = counterUri;
		this.variationCounter = variationCounter;
		this.rankUri = rankUri;
		this.variationRank = variationRank;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStatisticUri() {
		return statisticUri;
	}

	public void setStatisticUri(String statisticUri) {
		this.statisticUri = statisticUri;
	}

	public Resource getOrganizationUri() {
		return organizationUri;
	}

	public void setOrganizationUri(Resource organizationUri) {
		this.organizationUri = organizationUri;
	}

	public String getCounterUri() {
		return counterUri;
	}

	public void setCounterUri(String counterUri) {
		this.counterUri = counterUri;
	}

	public int getVariationCounter() {
		return variationCounter;
	}

	public void setVariationCounter(int variationCounter) {
		this.variationCounter = variationCounter;
	}

	public String getAggregateAmountUri() {
		return aggregateAmountUri;
	}

	public void setAggregateAmountUri(String aggregateAmountUri) {
		this.aggregateAmountUri = aggregateAmountUri;
	}

	public float getVariationAggregatedAmount() {
		return variationAggregatedAmount;
	}

	public void setVariationAggregatedAmount(float variationAggregatedAmount) {
		this.variationAggregatedAmount = variationAggregatedAmount;
	}

	public String getRankUri() {
		return rankUri;
	}

	public void setRankUri(String rankUri) {
		this.rankUri = rankUri;
	}

	public int getVariationRank() {
		return variationRank;
	}

	public void setVariationRank(int variationRank) {
		this.variationRank = variationRank;
	}

}