package objects;

import java.util.Comparator;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class StatisticAcrDtls {

	private String category = null;
	private Resource organization = null;
	private String counter = null;
	private float aggregatedAmount= -1;
	private int rank = -1;

	public StatisticAcrDtls(String category, Resource organization, String counter, float aggregatedAmount) {
		
		this.category = category;
		this.organization = organization;
		this.counter = counter;
		this.aggregatedAmount = aggregatedAmount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Resource getOrganization() {
		return organization;
	}

	public void setOrganization(Resource organization) {
		this.organization = organization;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public float getAggregatedAmount() {
		return aggregatedAmount;
	}

	public void setAggregatedAmount(float aggregatedAmount) {
		this.aggregatedAmount = aggregatedAmount;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public static Comparator<StatisticAcrDtls> getComparator() {
		return COMPARATOR;
	}
	
	public final static Comparator<StatisticAcrDtls> COMPARATOR = new Comparator<StatisticAcrDtls>() {
		public int compare(StatisticAcrDtls o1, StatisticAcrDtls o2) {
			return (int) (o2.getAggregatedAmount() - o1.getAggregatedAmount());
		}
	};

}